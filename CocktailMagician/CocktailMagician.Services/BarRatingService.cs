﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services
{
    public class BarRatingService : IBarRatingService
    {
        private readonly CMDbContext dbContext;
        private readonly IDTOMapper<BarRating, BarRatingDTO> dtoMapper;
        private readonly IFactory factory;

        public BarRatingService(CMDbContext dbContext, IDTOMapper<BarRating, BarRatingDTO> dtoMapper, IFactory factory)
        {
            this.dbContext = dbContext;
            this.dtoMapper = dtoMapper;
            this.factory = factory;
        }
        public async Task<BarRatingDTO> CreateBarRatingAsync(BarRatingDTO newBarRating)
        {
            if (newBarRating == null)
            {
                throw new ArgumentNullException();
            }
            if (this.dbContext.BarRatings.Any(br => br.UserId == newBarRating.UserId && br.BarId == newBarRating.BarId))
            {
               await UpdateRatingAsync(newBarRating);
            }
            else
            {
                var barRating = this.factory.CreateBarRating(newBarRating.Rating);

                barRating.UserId = newBarRating.UserId;
                barRating.BarId = newBarRating.BarId;

                await this.dbContext.BarRatings.AddAsync(barRating);
            }
            var bar = await UpdateBarRatingAsync(newBarRating.BarId);
            
            await this.dbContext.SaveChangesAsync();

            return newBarRating;
        }

        public async Task<ICollection<BarRatingDTO>> GetAllBarRatingsAsync(Guid barId)
        {
            var barRatings = await this.dbContext.BarRatings
                .Include(barRating => barRating.User)
                .Include(barRating => barRating.Bar)
                .Where(barRating => barRating.BarId == barId)
                .ToListAsync();

            var barRatingsDTO = this.dtoMapper.MapToDTO(barRatings);

            return barRatingsDTO;
        }
        private async Task<BarRating> UpdateRatingAsync(BarRatingDTO newBarRating)
        {
           var barRating = await this.dbContext.BarRatings
                .FirstOrDefaultAsync(br => br.BarId == newBarRating.BarId && br.UserId == newBarRating.UserId);
            barRating.Rating = newBarRating.Rating;
            return barRating;
        }

        private async Task<Bar> UpdateBarRatingAsync(Guid id)
        {
            var bar = await this.dbContext.Bars.Where(b => b.IsDeleted == false)
                .Include(b => b.Ratings)
                .FirstOrDefaultAsync(b => b.Id == id);

            bar.AverageRating = bar.Ratings.Average(r => r.Rating);
            
            return bar;
        }
    }
}
