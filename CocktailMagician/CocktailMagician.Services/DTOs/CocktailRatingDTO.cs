﻿using System;

namespace CocktailMagician.Services.DTOs
{
    public class CocktailRatingDTO
    {
        public Guid CocktailId { get; set; }
        public string Cocktail { get; set; }
        public int Rating { get; set; }
        public Guid UserId { get; set; }
        public string User { get; set; }
    }
}