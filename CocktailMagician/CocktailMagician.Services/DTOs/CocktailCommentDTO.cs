﻿using System;

namespace CocktailMagician.Services.DTOs
{
    public class CocktailCommentDTO
    {
        public Guid Id { get; set; }
        public Guid CocktailId { get; set; }
        public string Cocktail { get; set; }
        public string Text { get; set; }
        public Guid UserId { get; set; }
        public string User { get; set; }
    }
}