﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Services.DTOs
{
    public class CocktailDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImgPath { get; set; }
        public DateTime CreatedOn { get; set; }
        public ICollection<IngredientDTO> Ingredients { get; set; } = new List<IngredientDTO>();
        public ICollection<CocktailCommentDTO> Comments { get; set; } = new List<CocktailCommentDTO>();
        public ICollection<CocktailRatingDTO> Ratings { get; set; }
        public ICollection<BarDTO> Bars { get; set; } = new List<BarDTO>();
        public double AverageRating { get; set; }
    }
}
