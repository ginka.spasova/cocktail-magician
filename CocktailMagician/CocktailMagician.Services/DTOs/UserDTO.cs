﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Services.DTOs
{
    public class UserDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<BarCommentDTO> BarComments { get; set; }
        public ICollection<BarRatingDTO> BarRatings { get; set; }
        public ICollection<CocktailCommentDTO> CocktailComments { get; set; }
        public ICollection<CocktailRatingDTO> CocktailRatings { get; set; }

        public bool IsAdmin { get; set; }
        public DateTimeOffset? BannedUntil { get; set; }
    }
}
