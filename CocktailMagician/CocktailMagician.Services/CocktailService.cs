﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services.Constants;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace CocktailMagician.Services
{
    public class CocktailService : ICocktailService
    {
        private readonly CMDbContext dbContext;
        private readonly IFactory factory;
        private readonly IDTOMapper<Cocktail, CocktailDTO> mapper;
        private readonly IDateTimeProvider dateTimeProvider;

        public CocktailService(CMDbContext dbContext, IFactory factory, IDTOMapper<Cocktail, CocktailDTO> mapper, IDateTimeProvider dateTimeProvider)
        {
            this.dbContext = dbContext;
            this.factory = factory;
            this.mapper = mapper;
            this.dateTimeProvider = dateTimeProvider;
        }
        public async Task<CocktailDTO> GetCocktailAsync(Guid id)
        {
            var cocktail = await this.dbContext.Cocktails
                .Where(c => c.IsDeleted == false)
                .Include(c => c.Bars).ThenInclude(bc => bc.Bar)
                .Include(c => c.Ingredients).ThenInclude(bc => bc.Ingredient)
                .Include(c => c.Comments).ThenInclude(co => co.User)
                .FirstOrDefaultAsync(c => c.Id == id);



            if (cocktail == null)
                throw new ArgumentNullException();

            var cocktailDTO = mapper.MapToDTO(cocktail);

            return cocktailDTO;
        }
        public async Task<CocktailDTO> CreateCocktailAsync(CocktailDTO newCocktail)
        {
            if (newCocktail == null)
                throw new ArgumentNullException();

            if (this.dbContext.Cocktails.Any(c => c.Name == newCocktail.Name && c.IsDeleted == false))
                throw new ArgumentException();

            var cocktail = this.factory.CreateCocktail(newCocktail.Name, newCocktail.Description);

            cocktail.ImgPath = newCocktail.ImgPath;
            await this.dbContext.Cocktails.AddAsync(cocktail);

            cocktail.Ingredients = newCocktail.Ingredients
                .Select(i => factory.CreateCocktailIngredient(cocktail.Id, i.Id))
                .ToList();

            await this.dbContext.CocktailIngredients.AddRangeAsync(cocktail.Ingredients);
            await this.dbContext.SaveChangesAsync();

            return newCocktail;

        }
        public async Task<CocktailDTO> EditCocktailAsync(CocktailDTO editedCocktail)
        {
            var cocktail = await this.dbContext.Cocktails
                .Where(c => c.IsDeleted == false)
                .FirstOrDefaultAsync(c => c.Id == editedCocktail.Id);
            if (cocktail == null)
            {
                throw new ArgumentNullException();
            }
            if (this.dbContext.Cocktails.Any(c => c.Name == editedCocktail.Name && c.IsDeleted == false && c.Id != cocktail.Id))
                throw new ArgumentException();

            cocktail.Name = editedCocktail.Name;
            cocktail.Description = editedCocktail.Description;
            if(editedCocktail.ImgPath != null)
            cocktail.ImgPath = editedCocktail.ImgPath;
            cocktail.ModifiedOn = dateTimeProvider.GetDateTime();


            this.dbContext.Update(cocktail);
            await this.dbContext.SaveChangesAsync();
            cocktail = await this.dbContext.Cocktails
                .Include(c => c.Ingredients)
                .ThenInclude(i => i.Ingredient)
                .Include(c => c.Bars)
                .ThenInclude(c => c.Bar)
                .FirstOrDefaultAsync(c => c.Id == cocktail.Id) ?? throw new ArgumentNullException();

            var newCocktailDTO = mapper.MapToDTO(cocktail);

            return newCocktailDTO;
        }
        public async Task<CocktailDTO> EditIngredientsAsync(CocktailDTO editedCocktail)
        {
            var cocktail = await this.dbContext.Cocktails
               .Where(c => c.IsDeleted == false)
               .Include(c => c.Ingredients)
               .FirstOrDefaultAsync(c => c.Id == editedCocktail.Id);
            if (cocktail == null)
            {
                throw new ArgumentNullException();
            }

            this.dbContext.CocktailIngredients.RemoveRange(cocktail.Ingredients);

            cocktail.Ingredients = editedCocktail.Ingredients
               .Select(i => factory.CreateCocktailIngredient(cocktail.Id, i.Id))
               .ToList();

            cocktail.ModifiedOn = dateTimeProvider.GetDateTime();

            await this.dbContext.CocktailIngredients.AddRangeAsync(cocktail.Ingredients);
            await this.dbContext.SaveChangesAsync();

            cocktail = await this.dbContext.Cocktails
                .Include(c => c.Ingredients)
                .ThenInclude(i => i.Ingredient)
                .Include(c => c.Bars)
                .ThenInclude(c => c.Bar)
                .FirstOrDefaultAsync(c => c.Id == cocktail.Id) ?? throw new ArgumentNullException();

            var cocktailDTO = mapper.MapToDTO(cocktail);
            return cocktailDTO;
        }
        public async Task<bool> DeleteCocktailAsync(Guid id)
        {
            var cocktail = await this.dbContext.Cocktails
                .Where(c => c.IsDeleted == false)
                .Include(c => c.Ingredients)
                .Include(c => c.Bars)
                .FirstOrDefaultAsync(c => c.Id == id);
            if (cocktail == null)
                return false;

            this.dbContext.BarCocktails.RemoveRange(cocktail.Bars);
            this.dbContext.CocktailIngredients.RemoveRange(cocktail.Ingredients);

            cocktail.IsDeleted = true;
            cocktail.DeletedOn = dateTimeProvider.GetDateTime();

            this.dbContext.Update(cocktail);
            await this.dbContext.SaveChangesAsync();

            return true;
        }
        public async Task<CocktailDTO> GetDeletedCocktailAsync(Guid id)
        {
            var cocktail = await this.dbContext.Cocktails
            .Where(c => c.IsDeleted == true)
            .Include(c => c.Ingredients)
            .Include(c => c.Bars)
            .FirstOrDefaultAsync(c => c.Id == id);


            if (cocktail == null)
                throw new ArgumentNullException();

            var cocktailDTO = mapper.MapToDTO(cocktail);

            return cocktailDTO;
        }
        public async Task<bool> RestoreCocktailAsync(Guid id)
        {
            var cocktail = await this.dbContext.Cocktails
                 .Where(c => c.IsDeleted == true)
                 .FirstOrDefaultAsync(c => c.Id == id);
            if (cocktail == null)
                return false;

            cocktail.IsDeleted = false;
            cocktail.DeletedOn = null;

            await this.dbContext.SaveChangesAsync();

            return true;
        }
        public async Task<PageTransferInfo<CocktailDTO>> GetAllCocktailsAsync(int page, int pageSize = ConstantHolder.CocktailsPageSize)
        {
            IQueryable<Cocktail> cocktails = this.dbContext.Cocktails
                .Where(c => c.IsDeleted == false);

            var returnPage = await PaginatedList<Cocktail>.CreateAsync(cocktails, page, pageSize);

            var cocktailDTOs = mapper.MapToDTO(returnPage);

            return cocktailDTOs;

        }
        public async Task<PageTransferInfo<CocktailDTO>> GetAllDeletedCocktailsAsync(int page)
        {
            IQueryable<Cocktail> cocktails = this.dbContext.Cocktails
               .Where(c => c.IsDeleted == true);

            var returnPage = await PaginatedList<Cocktail>.CreateAsync(cocktails, page, ConstantHolder.CocktailsPageSize);

            var cocktailDTOs = mapper.MapToDTO(returnPage);

            return cocktailDTOs;
        }
        public async Task<PageTransferInfo<CocktailDTO>> SortCocktailsAsync(string sortingOption, int page)
        {

            if (sortingOption == null)
            {
                sortingOption = "name";
            }
            IQueryable<Cocktail> cocktails = this.dbContext.Cocktails.Where(c => c.IsDeleted == false)
                .Include(c => c.Bars).ThenInclude(bc => bc.Bar)
                .Include(c => c.Ingredients).ThenInclude(bc => bc.Ingredient);

            switch (sortingOption)
            {
                case "name":
                    cocktails = cocktails.OrderBy(c => c.Name);
                    break;
                case "name_desc":
                    cocktails = cocktails.OrderByDescending(cocktail => cocktail.Name);
                    break;
                case "rating":
                    cocktails = cocktails.OrderBy(cocktail => cocktail.AverageRating);
                    break;
                case "rating_desc":
                    cocktails = cocktails.OrderByDescending(cocktail => cocktail.AverageRating);
                    break;
            }
           
            
            var returnPage = await PaginatedList<Cocktail>.CreateAsync(cocktails, page, ConstantHolder.CocktailsPageSize);

            var cocktailDTOs = mapper.MapToDTO(returnPage);
            return cocktailDTOs;
        }
        public async Task<PageTransferInfo<CocktailDTO>> FilterCocktailsAsync(string filterOption, int page)
        {
            IQueryable<Cocktail> cocktails = this.dbContext.Cocktails
                .Where(c => c.IsDeleted == false)
                .Include(c => c.Bars).ThenInclude(bc => bc.Bar)
                .Include(c => c.Ingredients).ThenInclude(bc => bc.Ingredient);

            cocktails = cocktails.Where(c => c.Name.Contains(filterOption) 
            || c.Ingredients.Any(i => i.Ingredient.Name.Contains(filterOption)));

            var returnPage = await PaginatedList<Cocktail>.CreateAsync(cocktails, page, ConstantHolder.CocktailsPageSize);

            var cocktailDTOs = mapper.MapToDTO(returnPage);
            return cocktailDTOs;
        }
        public async Task<ICollection<CocktailDTO>> GetTopThreeCocktailsAsync()
        {
            var topCocktails = await this.dbContext.Cocktails
                            .Where(c => c.IsDeleted == false)
                            .Include(c => c.Bars).ThenInclude(bc => bc.Bar)
                            .Include(c => c.Ingredients).ThenInclude(bc => bc.Ingredient)
                            .OrderByDescending(c => c.AverageRating)
                            .Take(3)
                            .ToListAsync();

            var cocktailsDTO = this.mapper.MapToDTO(topCocktails);

            return cocktailsDTO;
        }
    }
}
