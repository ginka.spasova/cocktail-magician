﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Services.DTOMappers
{
    public class BarMapper : IDTOMapper<Bar, BarDTO>
    {
        public BarDTO MapToDTO(Bar bar)
        {
            if (bar == null)
                throw new ArgumentNullException();

            var barDTO = new BarDTO
            {
                Name = bar.Name,
                Id = bar.Id,
                Description = bar.Description,
                ImgPath = bar.ImgPath,
                Address = bar.Address,
                AverageRating = bar.AverageRating,
                Cocktails = bar.Cocktails.Select(c => new CocktailDTO
                {
                    Name = c.Cocktail.Name,
                    Id = c.CocktailId,
                    ImgPath = c.Cocktail.ImgPath
                }).ToList(),
                Comments = bar.Comments.Select(co => new BarCommentDTO
                { 
                    BarId = bar.Id,
                    Text = co.Text,
                    User = co.User.UserName,
                    UserId = co.UserId,
                    Id = co.Id
                }).ToList(),
                Phone = bar.Phone
            };
            return barDTO;
        }

        public ICollection<BarDTO> MapToDTO(ICollection<Bar> bars)
        {
            var barDTOs = bars.Select(b => MapToDTO(b)).ToList();
            return barDTOs;
        }

        public PageTransferInfo<BarDTO> MapToDTO(PaginatedList<Bar> barsPage)
        {
            var dtoPage = new PageTransferInfo<BarDTO>();
            dtoPage.TotalPages = barsPage.TotalPages;
            dtoPage.PageIndex = barsPage.PageIndex;
            dtoPage.HasNextPage = barsPage.HasNextPage;
            dtoPage.HasPreviousPage = barsPage.HasPreviousPage;
            dtoPage.Collection = barsPage.Select(i => MapToDTO(i)).ToList();

            return dtoPage;
        }
    }
}
