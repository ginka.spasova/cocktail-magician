﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services.Contracts
{
    public interface IBarService
    {
        Task<BarDTO> GetBarAsync(Guid id);
        Task<BarDTO> CreateBarAsync(BarDTO newBar);
        Task<BarDTO> EditBarAsync(Guid id, string newName, string newDescription, string newPhone, string newAddress, string imgPath);
        Task<BarDTO> AddAndRemoveCocktailsAsync(BarDTO changedBar);
        Task<bool> DeleteBarAsync(Guid id);
        Task<BarDTO> GetDeletedBarAsync(Guid id);
        Task<PageTransferInfo<BarDTO>> GetAllDeletedBarsAsync(int page);
        Task<bool> RestoreBarAsync(Guid id);
        Task<PageTransferInfo<BarDTO>> GetAllBarsAsync(int page);
        Task<PageTransferInfo<BarDTO>> SortBarsAsync(string sortingOption, int page);
        Task<PageTransferInfo<BarDTO>> FilterBarsAsync(string filterOption, int page);
        Task<ICollection<BarDTO>> GetTopThreeBarsAsync();

    }
}
