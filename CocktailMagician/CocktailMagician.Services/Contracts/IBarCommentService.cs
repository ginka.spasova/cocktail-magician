﻿using CocktailMagician.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services.Contracts
{
    public interface IBarCommentService
    {
        Task<BarCommentDTO> CreateBarCommentAsync(BarCommentDTO newBarComment);
        Task<BarCommentDTO> EditBarCommentAsync(Guid id, string newComment);
        Task<bool> DeleteBarCommentAsync(Guid id);
    }
}
