﻿using CocktailMagician.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services.Contracts
{
    public interface ICocktailRatingService
    {
        public Task<ICollection<CocktailRatingDTO>> GetAllCocktailRatingsAsync(Guid cocktailId);

        public Task<CocktailRatingDTO> CreateCocktailRatingAsync(CocktailRatingDTO cocktailRatingDTO);
    }
}
