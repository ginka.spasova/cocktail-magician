﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.ViewModelMappers
{
    public class BarCommentViewModelMapper : IViewModelMapper<BarCommentDTO, BarCommentViewModel>
    {
        public BarCommentDTO MapToDTO(BarCommentViewModel viewModel)
        {
            var barCommentDTO = new BarCommentDTO
            {
                Id = viewModel.Id,
                BarId = viewModel.BarId,
                Text = viewModel.Text,
                UserId = viewModel.UserId
            } ?? throw new ArgumentNullException();

            return barCommentDTO;
        }

        public BarCommentViewModel MapToViewModel(BarCommentDTO barCommentDTO)
        {
            if (barCommentDTO == null)
            {
                throw new ArgumentNullException();
            }
            
            var barCommentViewModel = new BarCommentViewModel
            {
                Id = barCommentDTO.Id,
                Bar = barCommentDTO.Bar,
                BarId = barCommentDTO.BarId,
                Text = barCommentDTO.Text,
                User = barCommentDTO.User,
                UserId = barCommentDTO.UserId
            };
            
            return barCommentViewModel;
        }

        public ICollection<BarCommentViewModel> MapToViewModel(ICollection<BarCommentDTO> barCommentsDTO)
        {
            var barCommentsViewModel = barCommentsDTO.Select(commentDTO => MapToViewModel(commentDTO)).ToList();
            
            return barCommentsViewModel;
        }

        public PageTransferInfo<BarCommentViewModel> MapToViewModel(PageTransferInfo<BarCommentDTO> commentsPage)
        {
            var vmPage = new PageTransferInfo<BarCommentViewModel>();
            vmPage.TotalPages = commentsPage.TotalPages;
            vmPage.PageIndex = commentsPage.PageIndex;
            vmPage.HasNextPage = commentsPage.HasNextPage;
            vmPage.HasPreviousPage = commentsPage.HasPreviousPage;
            vmPage.Collection = commentsPage.Collection.Select(commentDTO => MapToViewModel(commentDTO)).ToList();

            return vmPage;
        }


    }
}
