﻿using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.ViewModelMappers.Contracts
{
    public interface IViewModelMapper<TDTO, TViewModel>
    {
        TViewModel MapToViewModel(TDTO dtoEntity);
        ICollection<TViewModel> MapToViewModel(ICollection<TDTO> dtoEntities);
        PageTransferInfo<TViewModel> MapToViewModel(PageTransferInfo<TDTO> dtosPage);
        TDTO MapToDTO(TViewModel viewModel);
    }
}
