﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.ViewModelMappers
{
    public class CocktailRatingViewModelMapper : IViewModelMapper<CocktailRatingDTO, CocktailRatingViewModel>
    {
        public CocktailRatingDTO MapToDTO(CocktailRatingViewModel viewModel)
        {
            var cocktailRatingDTO = new CocktailRatingDTO
            {
                CocktailId = viewModel.CocktailId,
                Rating = viewModel.Rating,
                UserId = viewModel.UserId
            } ?? throw new ArgumentNullException();

            return cocktailRatingDTO;
        }

        public CocktailRatingViewModel MapToViewModel(CocktailRatingDTO cocktailRatingDTO)
        {
            if (cocktailRatingDTO == null)
            {
                throw new ArgumentNullException();
            }

            var cocktailRatingViewModel = new CocktailRatingViewModel
            {
                Cocktail = cocktailRatingDTO.Cocktail,
                CocktailId = cocktailRatingDTO.CocktailId,
                Rating = cocktailRatingDTO.Rating,
                User = cocktailRatingDTO.User,
                UserId = cocktailRatingDTO.UserId
            };

            return cocktailRatingViewModel;
        }

        public ICollection<CocktailRatingViewModel> MapToViewModel(ICollection<CocktailRatingDTO> cocktailRatingsDTO)
        {
            var cocktailRatingsViewModel = cocktailRatingsDTO.Select(ratingDTO => MapToViewModel(ratingDTO)).ToList();

            return cocktailRatingsViewModel;
        }

        public PageTransferInfo<CocktailRatingViewModel> MapToViewModel(PageTransferInfo<CocktailRatingDTO> ratingsPage)
        {
            var vmPage = new PageTransferInfo<CocktailRatingViewModel>();
            vmPage.TotalPages = ratingsPage.TotalPages;
            vmPage.PageIndex = ratingsPage.PageIndex;
            vmPage.HasNextPage = ratingsPage.HasNextPage;
            vmPage.HasPreviousPage = ratingsPage.HasPreviousPage;
            vmPage.Collection = ratingsPage.Collection.Select(ratingDTO => MapToViewModel(ratingDTO)).ToList();

            return vmPage;
        }
    }
}
