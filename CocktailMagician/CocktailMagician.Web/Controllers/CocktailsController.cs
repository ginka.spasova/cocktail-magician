﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Models;
using CocktailMagician.Services.Providers;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using CocktailMagician.Web.Utilities.Constants;
using NToastNotify;
using Microsoft.VisualBasic;

namespace CocktailMagician.Web.Controllers
{
    public class CocktailsController : Controller
    {
        private readonly ICocktailService service;
        private readonly IIngredientService ingredientService;
        private readonly IViewModelMapper<CocktailDTO, CocktailViewModel> mapper;
        private readonly IViewModelMapper<IngredientDTO, IngredientViewModel> ingredientMapper;
        private readonly IImageStorageProvider imageStorage;
        private readonly IToastNotification toastNotification;

        public CocktailsController(ICocktailService service, IIngredientService ingredientService,
            IViewModelMapper<CocktailDTO, CocktailViewModel> mapper, IViewModelMapper<IngredientDTO, IngredientViewModel> ingredientMapper,
            IImageStorageProvider imageStorage, IToastNotification toastNotification)
        {
            this.service = service;
            this.ingredientService = ingredientService;
            this.mapper = mapper;
            this.ingredientMapper = ingredientMapper;
            this.imageStorage = imageStorage;
            this.toastNotification = toastNotification;
        }

        public async Task<IActionResult> Index(string filterOption, string sortOption, int page = 1)
        {
            PageTransferInfo<CocktailDTO> cocktails;
            ViewBag.CurrentFilter = filterOption;
            ViewBag.CurrentSort = sortOption;
            ViewBag.SortByName = sortOption == "name" ? "name_desc" : "name";
            ViewBag.SortByRating = sortOption == "rating" ? "rating_desc" : "rating";
            if (!String.IsNullOrEmpty(filterOption))
            {
                cocktails = await this.service.FilterCocktailsAsync(filterOption, page);
            }
            else if (!String.IsNullOrEmpty(sortOption))
            {
                cocktails = await this.service.SortCocktailsAsync(sortOption, page);
            }
            else
            {
                cocktails = await this.service.GetAllCocktailsAsync(page);
            }
            var cocktailsViewModel = this.mapper.MapToViewModel(cocktails);


            return View(cocktailsViewModel);
        }

        public async Task<IActionResult> Details(Guid? id)
        {
            try
            {
                var cocktail = await this.service.GetCocktailAsync(id.Value);

                var cocktailViewModel = this.mapper.MapToViewModel(cocktail);

                return View(cocktailViewModel);

            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create()
        {
            var ingredients = await this.ingredientService.GetAllIngredientsAsync(1, int.MaxValue);
            var ingredientViews = ingredientMapper.MapToViewModel(ingredients);
            var cocktailCreationViewModel = new CocktailCreationViewModel { AllIngredients = ingredientViews.Collection };
            return View(cocktailCreationViewModel);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] CocktailCreationViewModel cocktailCreationViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (cocktailCreationViewModel.File != null)
                    {
                        var imagePath = await this.imageStorage.StoreImage(cocktailCreationViewModel.File, "cocktails");
                        cocktailCreationViewModel.ImgPath = imagePath;
                    }
                    else
                    {
                        cocktailCreationViewModel.ImgPath = ConstantsHolder.DefaultCocktailImg;
                    }
                    var cocktailViewModel = new CocktailViewModel
                    {
                        Name = cocktailCreationViewModel.Name,
                        Description = cocktailCreationViewModel.Description,
                        ImgPath = cocktailCreationViewModel.ImgPath,
                        Ingredients = cocktailCreationViewModel.IngredientIds
                            .Select(x => new IngredientViewModel { Id = x }).ToList()
                    };
                    var cocktailDTO = this.mapper.MapToDTO(cocktailViewModel);
                    var cocktail = await this.service.CreateCocktailAsync(cocktailDTO);

                    cocktailViewModel = this.mapper.MapToViewModel(cocktail);
                    this.toastNotification.AddSuccessToastMessage(ConstantsHolder.CreatedCocktail);
                    return RedirectToAction(nameof(Index));
                }
                catch (ArgumentException)
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.CocktailExists);
                    return View(cocktailCreationViewModel);
                }
                catch (InvalidDataException)
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.InvalidFiletype);
                    return View(cocktailCreationViewModel);
                }
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidInput);
            return View(cocktailCreationViewModel);
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(Guid? id)
        {
            try
            {
                var cocktail = await this.service.GetCocktailAsync(id.Value);

                var cocktailViewModel = this.mapper.MapToViewModel(cocktail);

                var cocktailEditViewModel = new CocktailEditViewModel { Cocktail = cocktailViewModel };

                return View(cocktailEditViewModel);

            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind] CocktailEditViewModel cocktailEditViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (cocktailEditViewModel.File != null)
                    {
                        var imagePath = await this.imageStorage.StoreImage(cocktailEditViewModel.File, "cocktails");
                        cocktailEditViewModel.Cocktail.ImgPath = imagePath;
                    }

                    var cocktailDTO = this.mapper.MapToDTO(cocktailEditViewModel.Cocktail);
                    var cocktail = await this.service.EditCocktailAsync(cocktailDTO);
                    var cocktailViewModel = this.mapper.MapToViewModel(cocktailDTO);
                    this.toastNotification.AddSuccessToastMessage(ConstantsHolder.CocktailEdited);
                    return RedirectToAction(nameof(Index));
                }
                catch (ArgumentException)
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.CocktailExists);
                    return View(cocktailEditViewModel);
                }
                catch (InvalidDataException)
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.InvalidFiletype);
                    return View(cocktailEditViewModel);
                }
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidInput);
            return View(cocktailEditViewModel);
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> EditIngredients(Guid? id)
        {
            try
            {
                var cocktail = await this.service.GetCocktailAsync(id.Value);

                var cocktailViewModel = this.mapper.MapToViewModel(cocktail);

                var ingredients = await this.ingredientService.GetAllIngredientsAsync(1, int.MaxValue);
                var ingredientViews = ingredientMapper.MapToViewModel(ingredients);
                var creationModel = new CocktailCreationViewModel
                {
                    Name = cocktailViewModel.Name,
                    Id = cocktailViewModel.Id,
                    ImgPath = cocktailViewModel.ImgPath,
                    Description = cocktailViewModel.Description,
                    AllIngredients = ingredientViews.Collection,
                    IngredientIds = cocktailViewModel.Ingredients.Select(c => c.Id).ToList()
                };
                return View(creationModel);

            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditIngredients([Bind] CocktailCreationViewModel cocktailCreationViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var cocktailViewModel = new CocktailViewModel
                    {
                        Name = cocktailCreationViewModel.Name,
                        Description = cocktailCreationViewModel.Description,
                        ImgPath = cocktailCreationViewModel.ImgPath,
                        Id = cocktailCreationViewModel.Id,
                        Ingredients = cocktailCreationViewModel.IngredientIds
                   .Select(x => new IngredientViewModel { Id = x }).ToList()
                    };
                    var cocktailDTO = this.mapper.MapToDTO(cocktailViewModel);
                    var cocktail = await this.service.EditIngredientsAsync(cocktailDTO);

                    cocktailViewModel = this.mapper.MapToViewModel(cocktail);
                    this.toastNotification.AddSuccessToastMessage(ConstantsHolder.CocktailEdited);
                    return RedirectToAction("Details", new { id = cocktailViewModel.Id });
                }
                catch
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.GeneralErrorMessage);
                    return View(cocktailCreationViewModel);
                }
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidInput);
            return View(cocktailCreationViewModel);
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(Guid? id)
        {
            try
            {
                var cocktail = await this.service.GetCocktailAsync(id.Value);

                var cocktailViewModel = this.mapper.MapToViewModel(cocktail);

                return View(cocktailViewModel);

            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {

            var result = await this.service.DeleteCocktailAsync(id);
            if (result)
                this.toastNotification.AddSuccessToastMessage(ConstantsHolder.CocktailDeleted);
            else
                this.toastNotification.AddErrorToastMessage(ConstantsHolder.CocktailNotDeleted);


            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Deleted(int page = 1)
        {
            var cocktails = await this.service.GetAllDeletedCocktailsAsync(page);
            var cocktailsViewModel = this.mapper.MapToViewModel(cocktails);
            return View(cocktailsViewModel);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DetailsDeleted(Guid? id)
        {
            try
            {
                var cocktail = await this.service.GetDeletedCocktailAsync(id.Value);

                var cocktailViewModel = this.mapper.MapToViewModel(cocktail);

                return View(cocktailViewModel);

            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Restore(Guid id)
        {
            var result = await this.service.RestoreCocktailAsync(id);
            if (result)
                this.toastNotification.AddSuccessToastMessage(ConstantsHolder.CocktailRestored);
            else
                this.toastNotification.AddErrorToastMessage(ConstantsHolder.CocktailNotRestored);

            return RedirectToAction(nameof(Index));
        }

    }
}
