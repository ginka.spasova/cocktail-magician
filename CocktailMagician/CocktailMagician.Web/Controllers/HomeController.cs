﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CocktailMagician.Web.Models;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using CocktailMagician.Services.DTOs;

namespace CocktailMagician.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICocktailService cocktailService;
        private readonly IViewModelMapper<CocktailDTO, CocktailViewModel> cocktailViewModelMapper;
        private readonly IBarService barService;
        private readonly IViewModelMapper<BarDTO, BarViewModel> barViewModelMapper;

        public HomeController(ILogger<HomeController> logger,
                              ICocktailService cocktailService,
                              IViewModelMapper<CocktailDTO, CocktailViewModel> cocktailViewModelMapper,
                              IBarService barService,
                              IViewModelMapper<BarDTO, BarViewModel> barViewModelMapper)
        {
            _logger = logger;
            this.cocktailService = cocktailService;
            this.cocktailViewModelMapper = cocktailViewModelMapper;
            this.barService = barService;
            this.barViewModelMapper = barViewModelMapper;
        }

        public async Task<IActionResult> Index()
        {
            var topBars = await this.barService.GetTopThreeBarsAsync();
            var topCocktails = await this.cocktailService.GetTopThreeCocktailsAsync();

            var topBarsViewModel = this.barViewModelMapper.MapToViewModel(topBars);
            var topCocktailsViewModel = this.cocktailViewModelMapper.MapToViewModel(topCocktails);

            var homeView = new HomeViewModel
            {
                TopThreeBars = topBarsViewModel,
                TopThreeCocktails = topCocktailsViewModel
            };

            return View(homeView);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult PageNotFound()
        {
            return View();
        }

    }
}
