﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Areas.Admin.Models;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.Models;
using CocktailMagician.Web.ViewModelMappers;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Utilities.Registration
{
    public static class ViewModelMappersRegistration
    {
        public static IServiceCollection AddViewModelMappers(this IServiceCollection services)
        {
            services.AddScoped<IViewModelMapper<IngredientDTO, IngredientViewModel>, IngredientViewModelMapper>();

            services.AddScoped<IViewModelMapper<CocktailDTO, CocktailViewModel>, CocktailViewModelMapper>();
            services.AddScoped<IViewModelMapper<CocktailCommentDTO, CocktailCommentViewModel>, CocktailCommentViewModelMapper>();
            services.AddScoped<IViewModelMapper<CocktailRatingDTO, CocktailRatingViewModel>, CocktailRatingViewModelMapper>();

            services.AddScoped<IViewModelMapper<BarDTO, BarViewModel>, BarViewModelMapper>();
            services.AddScoped<IViewModelMapper<BarCommentDTO, BarCommentViewModel>, BarCommentViewModelMapper>();
            services.AddScoped<IViewModelMapper<BarRatingDTO, BarRatingViewModel>, BarRatingViewModelMapper>();

            services.AddScoped<IViewModelMapper<UserDTO, UserViewModel>, UserViewModelMapper>();

            return services;
        }
    }
}
