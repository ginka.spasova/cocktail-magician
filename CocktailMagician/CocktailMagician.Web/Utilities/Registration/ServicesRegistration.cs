﻿using CocktailMagician.Services;
using CocktailMagician.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Utilities.Registration
{
    public static class ServicesRegistration
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IIngredientService, IngredientService>();
            services.AddScoped<IBarService, BarService>();
            services.AddScoped<IBarRatingService, BarRatingService>();
            services.AddScoped<IBarCommentService, BarCommentService>();
            services.AddScoped<ICocktailService, CocktailService>();
            services.AddScoped<ICocktailRatingService, CocktailRatingService>();
            services.AddScoped<ICocktailCommentService, CocktailCommentService>();
            services.AddScoped<IUserService, UserService>();

            return services;
        }
    }
}
