﻿using CocktailMagician.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Areas.Admin.Models
{
    [BindProperties(SupportsGet = true)]
    public class IngredientCreationViewModel
    {
        public IngredientViewModel IngredientViewModel { get; set; }
        public IFormFile File { get; set; }
    }
}
