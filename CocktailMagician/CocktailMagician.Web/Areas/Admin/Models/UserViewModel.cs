﻿using CocktailMagician.Web.Areas.Member.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Areas.Admin.Models
{
    public class UserViewModel
    {
        public string Name { get; set;}
        public Guid Id { get; set; }
        public bool IsAdmin { get; set; }
        public DateTimeOffset? BannedUntil { get; set; }
        public ICollection<BarCommentViewModel> BarComments { get; set; }
        public ICollection<BarRatingViewModel> BarRatings { get; set; }
        public ICollection<CocktailCommentViewModel> CocktailComments { get; set; }
        public ICollection<CocktailRatingViewModel> CocktailRatings { get; set; }
    }
}
