﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Areas.Member.Models
{
    public class CocktailRatingViewModel
    {
        public Guid CocktailId { get; set; }
        public string Cocktail { get; set; }
        public int Rating { get; set; }
        public Guid UserId { get; set; }
        public string User { get; set; }
    }
}
