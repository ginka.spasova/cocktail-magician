﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Areas.Member.Models;
using Microsoft.AspNetCore.Identity;
using CocktailMagician.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Routing;
using NToastNotify;
using CocktailMagician.Web.Utilities.Constants;
using Microsoft.VisualBasic;

namespace CocktailMagician.Web.Areas.Member.Controllers
{
    [Area("Member")]
    [Authorize]
    public class BarCommentsController : Controller
    {
        private readonly IBarCommentService barCommentService;
        private readonly IViewModelMapper<BarCommentDTO, BarCommentViewModel> viewModelMapper;
        private readonly UserManager<User> userManager;
        private readonly IToastNotification toastNotification;

        public BarCommentsController(IBarCommentService barCommentService,
                                     IViewModelMapper<BarCommentDTO, BarCommentViewModel> viewModelMapper,
                                     UserManager<User> userManager,
                                     IToastNotification toastNotification)
        {
            this.barCommentService = barCommentService;
            this.viewModelMapper = viewModelMapper;
            this.userManager = userManager;
            this.toastNotification = toastNotification;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] BarCommentViewModel barCommentViewModel)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var currentUser = await userManager.GetUserAsync(HttpContext.User);
                    barCommentViewModel.UserId = currentUser.Id;

                    var barCommentDTO = this.viewModelMapper.MapToDTO(barCommentViewModel);
                    await this.barCommentService.CreateBarCommentAsync(barCommentDTO);
                    this.toastNotification.AddSuccessToastMessage(ConstantsHolder.CommentAdded);
                    return RedirectToAction("Details", "Bars", new { area = "", id = barCommentViewModel.BarId });
                }
                catch
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.GeneralErrorMessage);
                    return RedirectToAction("Details", "Bars", new { id = barCommentViewModel.BarId, area = "" });
                }
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidComment);
            return RedirectToAction("Details", "Bars", new { id = barCommentViewModel.BarId, area = "" });
        }

        public async Task<IActionResult> Edit(BarCommentViewModel barCommentViewModel)
        {

            var currentUser = await userManager.GetUserAsync(HttpContext.User);
            if (currentUser.Id != barCommentViewModel.UserId)
            {
                return View(barCommentViewModel);
            }
            return RedirectToAction("Details", "Bars", new { area = "", id = barCommentViewModel.BarId });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind] BarCommentViewModel barCommentViewModel)
        {
            if (id != barCommentViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var newComment = barCommentViewModel.Text;
                    var comment = await this.barCommentService.EditBarCommentAsync(id, newComment);
                    barCommentViewModel = this.viewModelMapper.MapToViewModel(comment);

                    return RedirectToAction("Index", "Bars");
                }
                catch
                {
                    return NotFound();
                }
            }

            return View(barCommentViewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await this.barCommentService.DeleteBarCommentAsync(id);
            if (result)
                this.toastNotification.AddSuccessToastMessage(ConstantsHolder.CommentDelted);
            else
                this.toastNotification.AddErrorToastMessage(ConstantsHolder.CommentNotDeleted);

            return RedirectToAction("Index", "Bars", new { area = "" });
        }
    }
}
