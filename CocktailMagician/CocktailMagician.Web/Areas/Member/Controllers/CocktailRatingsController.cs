﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocktailMagician.Data.Models;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.Utilities.Constants;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;

namespace CocktailMagician.Web.Areas.Member.Controllers
{
    [Area("Member")]
    [Authorize]
    public class CocktailRatingsController : Controller
    {
        private readonly ICocktailRatingService service;
        private readonly IViewModelMapper<CocktailRatingDTO, CocktailRatingViewModel> viewModelMapper;
        private readonly UserManager<User> userManager;
        private readonly IToastNotification toastNotification;

        public CocktailRatingsController(ICocktailRatingService cocktailRatingService,
                                     IViewModelMapper<CocktailRatingDTO, CocktailRatingViewModel> viewModelMapper,
                                     UserManager<User> userManager, IToastNotification toastNotification)
        {
            this.service = cocktailRatingService;
            this.viewModelMapper = viewModelMapper;
            this.userManager = userManager;
            this.toastNotification = toastNotification;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] CocktailRatingViewModel cocktailRatingVM)
        {

            if (ModelState.IsValid)
            {
                var currentUser = await userManager.GetUserAsync(HttpContext.User);
                cocktailRatingVM.UserId = currentUser.Id;

                var cocktailRatingDTO = this.viewModelMapper.MapToDTO(cocktailRatingVM);

                await this.service.CreateCocktailRatingAsync(cocktailRatingDTO);
                this.toastNotification.AddSuccessToastMessage(ConstantsHolder.RatingAdded);
                return RedirectToAction("Details", "Cocktails", new { area = "", id = cocktailRatingVM.CocktailId });
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidInput);
            return View(cocktailRatingVM);
        }
    }
}
