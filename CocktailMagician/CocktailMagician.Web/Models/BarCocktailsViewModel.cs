﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Models
{
    public class BarCocktailsViewModel
    {
        public BarViewModel Bar { get; set; }

        public ICollection<CocktailViewModel> AllCocktails { get; set; }

        public ICollection<Guid> CocktailIds { get; set; }
    }
}
