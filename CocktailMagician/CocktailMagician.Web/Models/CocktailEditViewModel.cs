﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Models
{
    [BindProperties(SupportsGet = true)]
    public class CocktailEditViewModel
    {
        public CocktailViewModel Cocktail { get; set; }

        public IFormFile File { get; set; }
    }
}
