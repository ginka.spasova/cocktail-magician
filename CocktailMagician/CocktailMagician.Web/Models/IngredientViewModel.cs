﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Models
{
    [BindProperties(SupportsGet = true)]
    public class IngredientViewModel
    {
        public Guid Id { get; set; }

        [DisplayName("Ingredient Name")]
        [Required]
        [StringLength(30)]
        public string Name { get; set; }
        public ICollection<CocktailViewModel> Cocktails { get; set; } = new List<CocktailViewModel>();
        public string ImgPath { get; set; }
    }
}
