﻿using CocktailMagician.Data.Configurations;
using CocktailMagician.Data.Extensions;
using CocktailMagician.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace CocktailMagician.Data
{
    public class CMDbContext:IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public CMDbContext(DbContextOptions<CMDbContext> options) : base(options)
        {

        }
        protected CMDbContext()
        {

        }

        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Bar> Bars { get; set; }
        public DbSet<Cocktail> Cocktails { get; set; }
        public DbSet<BarComment> BarComments { get; set; }
        public DbSet<BarRating> BarRatings { get; set; }
        public DbSet<BarCocktail> BarCocktails { get; set; }
        public DbSet<CocktailComment> CocktailComments { get; set; }
        public DbSet<CocktailRating> CocktailRatings { get; set; }
        public DbSet<CocktailIngredient> CocktailIngredients { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new IngredientConfiguration());
            builder.ApplyConfiguration(new CocktailConfiguration());
            builder.ApplyConfiguration(new CocktailIngredientConfiguration());
            builder.ApplyConfiguration(new CocktailRatingConfiguration());
            builder.ApplyConfiguration(new CocktailCommentConfiguration());
            builder.ApplyConfiguration(new BarConfiguration());
            builder.ApplyConfiguration(new BarCommentConfiguration());
            builder.ApplyConfiguration(new BarRatingConfiguration());
            builder.ApplyConfiguration(new BarCocktailConfiguration());
            builder.ApplyConfiguration(new UserConfiguration());

            base.OnModelCreating(builder);
            builder.SeedData();
        }
    }
}
