﻿using CocktailMagician.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Data.Configurations
{
    public class BarCommentConfiguration : IEntityTypeConfiguration<BarComment>
    {
        public void Configure(EntityTypeBuilder<BarComment> builder)
        {
            builder.HasKey(comment => comment.Id);

            builder.Property(comment => comment.BarId)
                .IsRequired();

            builder.Property(comment => comment.UserId)
             .IsRequired();

            builder.Property(comment => comment.Text)
                .IsRequired();
        }
    }
}
