﻿using CocktailMagician.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Data.Configurations
{
    public class IngredientConfiguration : IEntityTypeConfiguration<Ingredient>
    {
        public void Configure(EntityTypeBuilder<Ingredient> builder)
        {
            builder.HasKey(ingredient => ingredient.Id);

            builder.Property(ingredient => ingredient.Name)
                .IsRequired();

            builder.Property(ingredient => ingredient.ImgPath);

            builder.HasMany(ingredient => ingredient.Cocktails)
                    .WithOne(cocktailIngredient => cocktailIngredient.Ingredient);
        }
    }
}
