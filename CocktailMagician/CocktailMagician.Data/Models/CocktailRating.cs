﻿using System;

namespace CocktailMagician.Data.Models
{
    public class CocktailRating
    {
        public Guid CocktailId { get; set; }

        public Cocktail Cocktail { get; set; }

        public int Rating { get; set; }

        public Guid UserId { get; set; }

        public User User { get; set; }
    }
}