﻿using CocktailMagician.Data.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CocktailMagician.Data.Models
{
    public class Cocktail : Entity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [DisplayName("Cocktail Name")]
        [StringLength(50)]
        public string Name { get; set; }
        [DisplayName("Cocktail Description")]
        [StringLength(500)]
        public string Description { get; set; }

        public string ImgPath { get; set; }

        public ICollection<CocktailIngredient> Ingredients { get; set; } = new List<CocktailIngredient>();

        public ICollection<CocktailComment> Comments { get; set; } = new List<CocktailComment>();

        public ICollection<CocktailRating> Ratings { get; set; } = new List<CocktailRating>();

        public ICollection<BarCocktail> Bars { get; set; } = new List<BarCocktail>();

        public double AverageRating { get; set; }
    }
}