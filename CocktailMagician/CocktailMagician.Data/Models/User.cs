﻿using CocktailMagician.Data.Models.Contracts;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace CocktailMagician.Data.Models
{
    public class User:IdentityUser<Guid>, IDeletable, IModifiable
    {
        public DateTime CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }

        public bool IsDeleted { get; set; }

        public ICollection<BarComment> BarComments { get; set; } = new List<BarComment>();

        public ICollection<BarRating> BarRatings { get; set; } = new List<BarRating>();

        public ICollection<CocktailComment> CocktailComments { get; set; } = new List<CocktailComment>();

        public ICollection<CocktailRating> CocktailRatings { get; set; } = new List<CocktailRating>();
    }
}