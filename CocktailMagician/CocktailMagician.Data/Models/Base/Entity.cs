﻿using CocktailMagician.Data.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Data.Models.Base
{
   public class Entity : IDeletable
    {
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }

        public bool IsDeleted { get; set; }
    }
}
