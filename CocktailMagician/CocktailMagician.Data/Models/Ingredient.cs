﻿using CocktailMagician.Data.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CocktailMagician.Data.Models
{
   public class Ingredient : Entity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [DisplayName("Ingredient Name")]
        [StringLength(30)]
        public string Name { get; set; }

        public ICollection<CocktailIngredient> Cocktails { get; set; } = new List<CocktailIngredient>();
        public string ImgPath { get; set; }
    }
}
