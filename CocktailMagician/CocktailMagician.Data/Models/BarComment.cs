﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CocktailMagician.Data.Models
{
   
    public class BarComment
    {
        public Guid Id { get; set; }
        public Guid BarId { get; set; }

        public Bar Bar { get; set; }

        [StringLength(500)]
        public string Text { get; set; }

        public Guid UserId { get; set; }

        public User User { get; set; }
    }
}