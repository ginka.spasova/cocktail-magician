﻿using CocktailMagician.Data.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CocktailMagician.Data.Models
{
    public class Bar : Entity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [DisplayName("Bar Name")]
        [StringLength(50)]
        public string Name { get; set; }
        public string ImgPath { get; set; }

        [DisplayName("Bar Description")]
        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        [MinLength(9)]
        public string Phone { get; set; }

        public ICollection<BarCocktail> Cocktails { get; set; } = new List<BarCocktail>();

        public ICollection<BarComment> Comments { get; set; } = new List<BarComment>();

        public ICollection<BarRating> Ratings { get; set; } = new List<BarRating>();

        public double AverageRating { get; set; }



    }
}