﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CocktailMagician.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Bars",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    ImgPath = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: false),
                    Address = table.Column<string>(maxLength: 100, nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    AverageRating = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bars", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cocktails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: false),
                    ImgPath = table.Column<string>(nullable: true),
                    AverageRating = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cocktails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ingredients",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    ImgPath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BarComments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    BarId = table.Column<Guid>(nullable: false),
                    Text = table.Column<string>(maxLength: 500, nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BarComments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BarComments_Bars_BarId",
                        column: x => x.BarId,
                        principalTable: "Bars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BarComments_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BarRatings",
                columns: table => new
                {
                    BarId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BarRatings", x => new { x.BarId, x.UserId });
                    table.ForeignKey(
                        name: "FK_BarRatings_Bars_BarId",
                        column: x => x.BarId,
                        principalTable: "Bars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BarRatings_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BarCocktails",
                columns: table => new
                {
                    CocktailId = table.Column<Guid>(nullable: false),
                    BarId = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BarCocktails", x => new { x.CocktailId, x.BarId });
                    table.ForeignKey(
                        name: "FK_BarCocktails_Bars_BarId",
                        column: x => x.BarId,
                        principalTable: "Bars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BarCocktails_Cocktails_CocktailId",
                        column: x => x.CocktailId,
                        principalTable: "Cocktails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CocktailComments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CocktailId = table.Column<Guid>(nullable: false),
                    Text = table.Column<string>(maxLength: 500, nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CocktailComments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CocktailComments_Cocktails_CocktailId",
                        column: x => x.CocktailId,
                        principalTable: "Cocktails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CocktailComments_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CocktailRatings",
                columns: table => new
                {
                    CocktailId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CocktailRatings", x => new { x.CocktailId, x.UserId });
                    table.ForeignKey(
                        name: "FK_CocktailRatings_Cocktails_CocktailId",
                        column: x => x.CocktailId,
                        principalTable: "Cocktails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CocktailRatings_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CocktailIngredients",
                columns: table => new
                {
                    IngredientId = table.Column<Guid>(nullable: false),
                    CocktailId = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CocktailIngredients", x => new { x.IngredientId, x.CocktailId });
                    table.ForeignKey(
                        name: "FK_CocktailIngredients_Cocktails_CocktailId",
                        column: x => x.CocktailId,
                        principalTable: "Cocktails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CocktailIngredients_Ingredients_IngredientId",
                        column: x => x.IngredientId,
                        principalTable: "Ingredients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Discriminator", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("1d195e6d-80cf-47eb-a840-2ddeb11734c9"), "2aafd5f3-1e0d-4b66-a347-b57e9171fffd", "Role", "Member", "MEMBER" },
                    { new Guid("469690a1-0da5-413d-b82f-f9ec5d96502d"), "37250154-1046-4cc0-99eb-ebf1b1914fe4", "Role", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CreatedOn", "DeletedOn", "Email", "EmailConfirmed", "IsDeleted", "LockoutEnabled", "LockoutEnd", "ModifiedOn", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("c39cbf0f-106c-426a-b3c1-f5fe94ada252"), 0, "e20b95f0-49e6-463d-a674-fc010d9feed4", new DateTime(2020, 5, 22, 11, 17, 50, 255, DateTimeKind.Local).AddTicks(8124), null, "harrypotter@cm.com", false, false, false, null, null, "HARRYPOTTER@CM.COM", "HARRYPOTTER@CM.COM", "AQAAAAEAACcQAAAAEPbXxosvTYgThD4CCLioQTrGF3HsX23Z6o5qM6kh6RIc6Slq7G2F0LYIFVUOe3E+LA==", null, false, "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN", false, "harrypotter@cm.com" },
                    { new Guid("25b9ec71-4db2-489e-9d55-a7a0a2b881e6"), 0, "919912a1-6140-405d-97bd-3e72b05bb934", new DateTime(2020, 5, 22, 11, 17, 50, 225, DateTimeKind.Local).AddTicks(8441), null, "gandalf@cm.com", false, false, false, null, null, "GANDALF@CM.COM", "GANDALF@CM.COM", "AQAAAAEAACcQAAAAEKwoxflnkDxvoOhY8Y4skZVOmqic+VJjKPNwne43EAAnqg3ZjOf97b/ZjuvwgKBbZQ==", null, false, "15CLJEKQCTLPRXMVXXNSWXZH6R6KJRRU", false, "gandalf@cm.com" }
                });

            migrationBuilder.InsertData(
                table: "Bars",
                columns: new[] { "Id", "Address", "AverageRating", "CreatedOn", "DeletedOn", "Description", "ImgPath", "IsDeleted", "ModifiedOn", "Name", "Phone" },
                values: new object[,]
                {
                    { new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), "Sofia 1408, Petko Karavelov 5", 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "RockIT Rock Bar is one of the best rock and metal music venues in Sofia. The bar has different halls - central with a stage where lifetime performances and DJ parties take place, and gambling halls with billiards, table football and other entertainment, interesting cuisine, beer appetizers and tempting desserts!", "/images/bars/rockit.jpg", false, null, "RockIt", "088 866 6991" },
                    { new Guid("d615102a-3d87-4a92-bace-ab92a6b0f10b"), "Plovdiv 4000, Blvd  6th of September 163", 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Great atmosphere, and relaxing music, a place to forget about bad things, take a break and enjoy life.", "/images/bars/tomato.jpg", false, null, "Tomato", "089 637 0777" },
                    { new Guid("c846cbee-50d3-41a1-a9bb-3ea2b9e39ca1"), "Sofia 1000, Blvd Bulgaria 1", 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Club *MIXTAPE 5* is one of the most popular party and concert venues in Sofia. It is located in the city central area, close to the National Palace of Culture. Since 2011 the club has hosted hundreds of live music events, DJ parties and performances featuring both local and foreign artists.", "/images/bars/mixtape.jpg", false, null, "MIXTAPE 5", "087 929 5965" },
                    { new Guid("d0173878-6397-4905-980b-865b3d609f70"), "Sofia 1164, Nerazdelni 14", 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Fuzzy is a cozy bar in the heart of the city with small stage for acoustic music, magic, comedy and other live events. It has it all - cocktails, craft beer and all kind of alcohol mixed with good music and vibe.", "/images/bars/fuzzy-bar.jpg", false, null, "Fuzzy bar", "088 349 3517" },
                    { new Guid("86d67f0e-a88d-4054-880a-a329feaf6110"), "Plovdiv 4000, Captain Raicho 50", 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "It is so simply surrounded by greenery and wherever you look you see friends!", "/images/bars/default-bar.jpeg", false, null, "Buddy's", "089 944 5356" },
                    { new Guid("5c0e4d04-7e42-4448-995d-24c351a31cd1"), "Sofia 1000, Georgi Sava Rakovski 108", 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The best night club in Sofia, located in the heart of the city. The club offers cozy atmosphere, very good service, very competitive prices and a very good quality of music. International DJ's are listed at least 4 times per month.", "/images/bars/carrusel-club.jpg", false, null, "Carrusel club", "088 996 9687" },
                    { new Guid("eaf089bf-e0ba-42b5-847a-44e57b1629c0"), "Plovdiv 4000, Evlogi Georgiev 5", 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "A place of mystery where no night is the same. The music as well as the people can surprise you at any moment. The only thing that's promised is the fun.", "/images/bars/no-sense.jpg", false, null, "No sense", "089 673 8654" },
                    { new Guid("64948c57-4c27-4e2a-8bf1-947973ea1015"), "Sunny Beach 8240", 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "An extravagant beach and night club, associated with high class clubbing entertainment. Bedroom beach is characterized by an elegant interior, made out of natural materials, stylish extended space, interesting inner architecture and decor, impressive electric boutique L-acoustic sound, first-class service and the most modern lifestyle mood that can be felt only by passing its entrance.", "/images/bars/bedroom-beach-club.jpg", false, null, "Bedroom beach club", "088 886 0666" },
                    { new Guid("e460a02c-437b-401d-8fa4-db824f956440"), "Golden Sands 9007, Mimoza Hotel", 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The Team Daiquiri bartenders adds acrobatic flair to mixology by juggling glasses and bottles while creating legendary Cocktails every night of the week. The bar proudly serves all sorts of tropical cocktails, frozen margaritas, and a long list of liqueurs, beers, shots and Specials.", "/images/bars/daiquiri-cocktail-bar.jpg", false, null, "Daiquiri Cocktail bar", "088 580 0275" },
                    { new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), "Sofia 1000, Graf Ignatiev 1", 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The home of good music and mood in Sofia has one name: Rock'n'Rolla. This bar and club is the best place to go if you love rock music and want to mingle with a crowd of friendly similarly-minded people.", "/images/bars/rock-n-rolla.jpg", false, null, "Rock'n'Rolla", "088 913 1344" }
                });

            migrationBuilder.InsertData(
                table: "Cocktails",
                columns: new[] { "Id", "AverageRating", "CreatedOn", "DeletedOn", "Description", "ImgPath", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { new Guid("516e12b9-cbee-4ae7-a408-2ad3825f5d28"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "On paper, the Long Island Iced Tea is one hot mess of a drink. Four different—and disparate—spirits slugging it out in a single glass, along with triple sec, lemon juice and cola? The recipe reads more like a frat house hazing than one of the world’s most popular cocktails. And yet, somehow, it works.", "/images/cocktails/long-island-iced-tea.jpg", false, null, "Long Island iced tea" },
                    { new Guid("bd11ea02-1179-410e-9728-479732c2ffe3"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "A White Russian cocktail is a fall and winter delight. Made with a coffee liqeur like Kahlua with milk or cream, it's a delicious sweet dessert drink. ", "/images/cocktails/white-russian.jpg", false, null, "White russian" },
                    { new Guid("8dc7b3dc-2577-4f88-8e7a-ff698a9d8c88"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The perfect summer cocktail to enjoy by the beach but tasty enough to drink all year round. Combine vodka with peach schnapps and cranberry juice to make a classic sex on the beach cocktail. Garnish with cocktail cherries and orange slices.", "/images/cocktails/sex-on-the-beach.jpg", false, null, "Sex on the beach" },
                    { new Guid("e6158613-4628-4d9b-8107-7046217e064d"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The daiquiri is a classic rum sour drink. It is also one of the freshest drinks you can find and an essential rum cocktail everyone should know and taste. ", "/images/cocktails/daiquiri.jpg", false, null, "Classic daiquiri" },
                    { new Guid("c88b565b-c9dd-44e8-9e84-a7158a796f8f"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The Tequila Sunrise, with its bright striations of color, evokes a summer sunrise. This simple classic has only three ingredients—tequila, grenadine and orange juice—and is served unmixed to preserve the color of each layer.", "/images/cocktails/tequila-sunrise.jpg", false, null, "Tequila Sunrise" },
                    { new Guid("4991f748-572f-4ecd-94d2-109d9ab13583"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Of all the classic cocktails in all the cocktail bars in all the world, the iconic Martini reigns supreme. Stunning in its simplicity and strikingly elegant when ordered with confidence, a perfect Martini showcases the sharp, botanical flavors of gin and vermouth with little to no interference.", "/images/cocktails/martini.jpg", false, null, "Martini" },
                    { new Guid("24647136-f565-46a2-abc8-0a80e7e34f07"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "It takes a good vodka drink to survive a Moscow winter, and the classic Black Russian cocktail definitely makes the cut.", "/images/cocktails/black-russian.jpg", false, null, "Black russian" },
                    { new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The Cuba Libre is a refreshing and incredibly simple mixed drink. It is similar to a rum and Coke, but there is a twist you will not want to miss.", "/images/cocktails/cuba-libre.jpg", false, null, "Cuba libre" },
                    { new Guid("201a5ee2-f06a-4c51-bd79-b7dcb206f80a"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The Jack and Coke is a classic, easygoing cocktail that blends Jack Daniels whiskey with cola. When smooth, charcoal-mellowed Tennessee Whiskey first met the sweet fizz of Coke, this classic cocktail was born.", "/images/cocktails/jack-and-coke.jpg", false, null, "Jack'n'Coke" },
                    { new Guid("60f207de-d14f-4797-a995-2b6ce559eaa4"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Served straight up or on the rocks, the margarita is one of the most popular cocktails of all time. And for good reason! It will cool you down on a hot day or warm you up on a cool day. Any day is a good day for a margarita.", "/images/cocktails/margarita.jpg", false, null, "Margarita" },
                    { new Guid("6a1297e5-a8c6-4d8a-bd9e-1d4b2fef7ebc"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The Strawberry Daiquiri is a classic summertime drink. Chill down with this frosty rum concoction.", "/images/cocktails/strawberry-daiquiri.jpg", false, null, "Strawberry daiquiri" },
                    { new Guid("e0f0044c-8f6e-4324-946c-77c76fa4cec3"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The Manhattan’s mix of American whiskey and Italian vermouth, perked up with a few dashes of aromatic bitters, is timeless and tasty—the very definition of what a cocktail should be.", "/images/cocktails/manhattan.jpg", false, null, "Manhattan" },
                    { new Guid("e55a6e1a-5e60-4b3c-a350-b7717bea9381"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Lipsmackingly sweet-and-sour, the Cosmopolitan cocktail of vodka, cranberry, orange liqueur and citrus is a good time in a glass. Perfect for a party.", "/images/cocktails/cosmopolitan.jpg", false, null, "Cosmopolitan" },
                    { new Guid("c4d90efc-a88a-40a5-a103-75e5bc2adafd"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "It's one of the most refreshing drinks you can mix up. Its combination of sweetness, citrus, and herbaceous mint flavors is intended to complement the rum, and has made the mojito a popular summer drink", "/images/cocktails/mojito.jpg", false, null, "Mojito" },
                    { new Guid("c1f9f9e9-108e-452b-a2a3-aeefd363a7eb"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "A classic cocktail, the Old Fashioned will always be a delicious bourbon drink that never goes out of style.", "/images/cocktails/oldfashioned.jpg", false, null, "Old fashioned" },
                    { new Guid("69d0e4d0-4a73-4bd9-87de-c98418f28933"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "A tropical blend of rich coconut cream, white rum and tangy pineapple - served with an umbrella for kitsch appeal.", "/images/cocktails/pina-colada.jpg", false, null, "Pina Colada" },
                    { new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), 0.0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The Bloody Mary is a vodka-soaked nutritional breakfast and hangover cure all in one. There's a reason this iconic cocktail is a classic - you really have to try it.", "/images/cocktails/bloody-mary.jpg", false, null, "Bloody Mary" }
                });

            migrationBuilder.InsertData(
                table: "Ingredients",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "ImgPath", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { new Guid("e85bacd8-d038-42cd-a66d-29c7da1450f1"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/pineapple-juice.jpg", false, null, "Pineaple juice" },
                    { new Guid("08765ef5-2763-419c-b660-29e4d5fcc469"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/gin.jpg", false, null, "Gin" },
                    { new Guid("3621efae-0cba-41fc-8fd6-19b5c2b5de2f"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/tequila.jpeg", false, null, "Tequila" },
                    { new Guid("17e39e62-2fd6-47cd-8270-a8eaef44c42a"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/orange-juice.jpg", false, null, "Orange juice" },
                    { new Guid("8e64f7ee-85c7-4324-a59c-74daaad9c0b8"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/grenadine.jpg", false, null, "Grenadine syrup" },
                    { new Guid("9fd5730a-d9c5-4f0b-9c01-9a9966070e08"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/worcestershire.jpg", false, null, "Worcestershire Sauce" },
                    { new Guid("a51c386e-57aa-48f7-aad9-626dfc641c0e"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/peach-schnapps.jpg", false, null, "Peach Schnapps" },
                    { new Guid("471d21df-564e-4697-8c32-e5a97e053444"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/coffee-liqueur.jpg", false, null, "Coffee Liqueur" },
                    { new Guid("6aa0f5e0-942e-4dd3-873c-50e6ef71120a"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/tabasco.jpg", false, null, "Tabasco" },
                    { new Guid("9ceda011-ee58-4aad-9d3a-95b82dd03817"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/tomatoJuice.jpg", false, null, "Tomato juice" },
                    { new Guid("1bf3150a-41db-44fb-85aa-c118fab86007"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/cream.jpg", false, null, "Cream" },
                    { new Guid("419d7e92-65a6-451d-8d05-ed39b19702c1"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/sugar-syrup.jpg", false, null, "Sugar syrup" },
                    { new Guid("726645d6-8628-4d43-9c9e-7a47b6fc3263"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/coca-cola.jpg", false, null, "Coca Cola" },
                    { new Guid("6e6d43ca-1378-4165-b882-fbf28cd0fcde"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/mint.jpg", false, null, "Mint" },
                    { new Guid("0bd60dfe-102a-46e0-be63-30741582d6f7"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/salt.jpg", false, null, "Salt" },
                    { new Guid("5c3b98d2-af05-47ab-9f0f-80788ab0cc47"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/sugar.jpg", false, null, "Sugar" },
                    { new Guid("857fa6be-8ac4-4442-8db1-9f9570c93458"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/strawberry.jpg", false, null, "Strawberry" },
                    { new Guid("9b4d2e3a-90ac-47d7-b314-5bcfe9955b8b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/lemon-juice.jpg", false, null, "Lemon juice" },
                    { new Guid("84b72c0f-5ef0-448f-a4d0-f1973f8a23e3"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/vermouth.jpg", false, null, "Vermouth" },
                    { new Guid("8e800e32-7d9f-4c7b-987e-dffe362ab8d7"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/cranberry-juice.jpg", false, null, "Cranberry juice" },
                    { new Guid("e26e3bcb-c810-4196-b0bf-55c8c824acbb"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/triple-sec.jpg", false, null, "Triple sec" },
                    { new Guid("d7a995af-c9e9-4fe2-843c-851314248b6c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/vodka.jpg", false, null, "Vodka" },
                    { new Guid("130807eb-01a3-47bc-a4b6-a66d8ee5502b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/lime-juice.jpg", false, null, "Lime juice" },
                    { new Guid("672fdcd1-a606-42e8-8ce1-397a92379704"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/white-rum.jpg", false, null, "White rum" },
                    { new Guid("54ff08c2-f064-47a2-bdde-cf3bd955d867"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/orange-slices.jpg", false, null, "Orange slices" },
                    { new Guid("52675120-68a9-4481-8fd5-32ef028cffbc"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/brown-sugar.jpeg", false, null, "Brown sugar" },
                    { new Guid("a3b05b48-6c99-4f8f-879b-e521dbca5322"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/angostura-bitters.jpg", false, null, "Angostura bitters" },
                    { new Guid("519495da-2ca5-41a2-b361-6ec190d43692"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/whiskey.jpg", false, null, "Whisky" },
                    { new Guid("87112b08-16ba-49fb-a2c6-0450cd44b42d"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/jack-daniels.png", false, null, "Jack Daniels" },
                    { new Guid("ca055b38-7d93-436d-904a-bc94c3f7cbd7"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "/images/ingredients/soda-water.jpg", false, null, "Soda water" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("c39cbf0f-106c-426a-b3c1-f5fe94ada252"), new Guid("469690a1-0da5-413d-b82f-f9ec5d96502d") },
                    { new Guid("25b9ec71-4db2-489e-9d55-a7a0a2b881e6"), new Guid("469690a1-0da5-413d-b82f-f9ec5d96502d") }
                });

            migrationBuilder.InsertData(
                table: "BarCocktails",
                columns: new[] { "CocktailId", "BarId", "DeletedOn", "IsDeleted" },
                values: new object[,]
                {
                    { new Guid("69d0e4d0-4a73-4bd9-87de-c98418f28933"), new Guid("e460a02c-437b-401d-8fa4-db824f956440"), null, false },
                    { new Guid("e6158613-4628-4d9b-8107-7046217e064d"), new Guid("e460a02c-437b-401d-8fa4-db824f956440"), null, false },
                    { new Guid("e6158613-4628-4d9b-8107-7046217e064d"), new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), null, false },
                    { new Guid("c88b565b-c9dd-44e8-9e84-a7158a796f8f"), new Guid("d615102a-3d87-4a92-bace-ab92a6b0f10b"), null, false },
                    { new Guid("c88b565b-c9dd-44e8-9e84-a7158a796f8f"), new Guid("eaf089bf-e0ba-42b5-847a-44e57b1629c0"), null, false },
                    { new Guid("c88b565b-c9dd-44e8-9e84-a7158a796f8f"), new Guid("64948c57-4c27-4e2a-8bf1-947973ea1015"), null, false },
                    { new Guid("c88b565b-c9dd-44e8-9e84-a7158a796f8f"), new Guid("e460a02c-437b-401d-8fa4-db824f956440"), null, false },
                    { new Guid("c88b565b-c9dd-44e8-9e84-a7158a796f8f"), new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), null, false },
                    { new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), null, false },
                    { new Guid("4991f748-572f-4ecd-94d2-109d9ab13583"), new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), null, false },
                    { new Guid("4991f748-572f-4ecd-94d2-109d9ab13583"), new Guid("c846cbee-50d3-41a1-a9bb-3ea2b9e39ca1"), null, false },
                    { new Guid("4991f748-572f-4ecd-94d2-109d9ab13583"), new Guid("5c0e4d04-7e42-4448-995d-24c351a31cd1"), null, false },
                    { new Guid("4991f748-572f-4ecd-94d2-109d9ab13583"), new Guid("64948c57-4c27-4e2a-8bf1-947973ea1015"), null, false },
                    { new Guid("4991f748-572f-4ecd-94d2-109d9ab13583"), new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), null, false },
                    { new Guid("69d0e4d0-4a73-4bd9-87de-c98418f28933"), new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), null, false },
                    { new Guid("69d0e4d0-4a73-4bd9-87de-c98418f28933"), new Guid("d615102a-3d87-4a92-bace-ab92a6b0f10b"), null, false },
                    { new Guid("69d0e4d0-4a73-4bd9-87de-c98418f28933"), new Guid("64948c57-4c27-4e2a-8bf1-947973ea1015"), null, false },
                    { new Guid("e6158613-4628-4d9b-8107-7046217e064d"), new Guid("64948c57-4c27-4e2a-8bf1-947973ea1015"), null, false },
                    { new Guid("e6158613-4628-4d9b-8107-7046217e064d"), new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), null, false },
                    { new Guid("8dc7b3dc-2577-4f88-8e7a-ff698a9d8c88"), new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), null, false },
                    { new Guid("8dc7b3dc-2577-4f88-8e7a-ff698a9d8c88"), new Guid("64948c57-4c27-4e2a-8bf1-947973ea1015"), null, false },
                    { new Guid("516e12b9-cbee-4ae7-a408-2ad3825f5d28"), new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), null, false },
                    { new Guid("516e12b9-cbee-4ae7-a408-2ad3825f5d28"), new Guid("64948c57-4c27-4e2a-8bf1-947973ea1015"), null, false },
                    { new Guid("516e12b9-cbee-4ae7-a408-2ad3825f5d28"), new Guid("d0173878-6397-4905-980b-865b3d609f70"), null, false },
                    { new Guid("24647136-f565-46a2-abc8-0a80e7e34f07"), new Guid("d615102a-3d87-4a92-bace-ab92a6b0f10b"), null, false },
                    { new Guid("24647136-f565-46a2-abc8-0a80e7e34f07"), new Guid("c846cbee-50d3-41a1-a9bb-3ea2b9e39ca1"), null, false },
                    { new Guid("24647136-f565-46a2-abc8-0a80e7e34f07"), new Guid("5c0e4d04-7e42-4448-995d-24c351a31cd1"), null, false },
                    { new Guid("24647136-f565-46a2-abc8-0a80e7e34f07"), new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), null, false },
                    { new Guid("24647136-f565-46a2-abc8-0a80e7e34f07"), new Guid("d0173878-6397-4905-980b-865b3d609f70"), null, false },
                    { new Guid("bd11ea02-1179-410e-9728-479732c2ffe3"), new Guid("d615102a-3d87-4a92-bace-ab92a6b0f10b"), null, false },
                    { new Guid("bd11ea02-1179-410e-9728-479732c2ffe3"), new Guid("5c0e4d04-7e42-4448-995d-24c351a31cd1"), null, false },
                    { new Guid("bd11ea02-1179-410e-9728-479732c2ffe3"), new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), null, false },
                    { new Guid("bd11ea02-1179-410e-9728-479732c2ffe3"), new Guid("d0173878-6397-4905-980b-865b3d609f70"), null, false },
                    { new Guid("8dc7b3dc-2577-4f88-8e7a-ff698a9d8c88"), new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), null, false },
                    { new Guid("8dc7b3dc-2577-4f88-8e7a-ff698a9d8c88"), new Guid("d615102a-3d87-4a92-bace-ab92a6b0f10b"), null, false },
                    { new Guid("8dc7b3dc-2577-4f88-8e7a-ff698a9d8c88"), new Guid("c846cbee-50d3-41a1-a9bb-3ea2b9e39ca1"), null, false },
                    { new Guid("8dc7b3dc-2577-4f88-8e7a-ff698a9d8c88"), new Guid("eaf089bf-e0ba-42b5-847a-44e57b1629c0"), null, false },
                    { new Guid("8dc7b3dc-2577-4f88-8e7a-ff698a9d8c88"), new Guid("86d67f0e-a88d-4054-880a-a329feaf6110"), null, false },
                    { new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), new Guid("d615102a-3d87-4a92-bace-ab92a6b0f10b"), null, false },
                    { new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), new Guid("c846cbee-50d3-41a1-a9bb-3ea2b9e39ca1"), null, false },
                    { new Guid("bd11ea02-1179-410e-9728-479732c2ffe3"), new Guid("c846cbee-50d3-41a1-a9bb-3ea2b9e39ca1"), null, false },
                    { new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), new Guid("86d67f0e-a88d-4054-880a-a329feaf6110"), null, false },
                    { new Guid("e55a6e1a-5e60-4b3c-a350-b7717bea9381"), new Guid("5c0e4d04-7e42-4448-995d-24c351a31cd1"), null, false },
                    { new Guid("e55a6e1a-5e60-4b3c-a350-b7717bea9381"), new Guid("64948c57-4c27-4e2a-8bf1-947973ea1015"), null, false },
                    { new Guid("e55a6e1a-5e60-4b3c-a350-b7717bea9381"), new Guid("d0173878-6397-4905-980b-865b3d609f70"), null, false },
                    { new Guid("c4d90efc-a88a-40a5-a103-75e5bc2adafd"), new Guid("d615102a-3d87-4a92-bace-ab92a6b0f10b"), null, false },
                    { new Guid("c4d90efc-a88a-40a5-a103-75e5bc2adafd"), new Guid("86d67f0e-a88d-4054-880a-a329feaf6110"), null, false },
                    { new Guid("c4d90efc-a88a-40a5-a103-75e5bc2adafd"), new Guid("64948c57-4c27-4e2a-8bf1-947973ea1015"), null, false },
                    { new Guid("c4d90efc-a88a-40a5-a103-75e5bc2adafd"), new Guid("e460a02c-437b-401d-8fa4-db824f956440"), null, false },
                    { new Guid("c1f9f9e9-108e-452b-a2a3-aeefd363a7eb"), new Guid("d615102a-3d87-4a92-bace-ab92a6b0f10b"), null, false },
                    { new Guid("e55a6e1a-5e60-4b3c-a350-b7717bea9381"), new Guid("eaf089bf-e0ba-42b5-847a-44e57b1629c0"), null, false },
                    { new Guid("c1f9f9e9-108e-452b-a2a3-aeefd363a7eb"), new Guid("c846cbee-50d3-41a1-a9bb-3ea2b9e39ca1"), null, false },
                    { new Guid("c1f9f9e9-108e-452b-a2a3-aeefd363a7eb"), new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), null, false },
                    { new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), null, false },
                    { new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), new Guid("d615102a-3d87-4a92-bace-ab92a6b0f10b"), null, false },
                    { new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), new Guid("c846cbee-50d3-41a1-a9bb-3ea2b9e39ca1"), null, false },
                    { new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), new Guid("eaf089bf-e0ba-42b5-847a-44e57b1629c0"), null, false },
                    { new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), new Guid("5c0e4d04-7e42-4448-995d-24c351a31cd1"), null, false },
                    { new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), null, false },
                    { new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), new Guid("eaf089bf-e0ba-42b5-847a-44e57b1629c0"), null, false },
                    { new Guid("c1f9f9e9-108e-452b-a2a3-aeefd363a7eb"), new Guid("5c0e4d04-7e42-4448-995d-24c351a31cd1"), null, false },
                    { new Guid("e55a6e1a-5e60-4b3c-a350-b7717bea9381"), new Guid("c846cbee-50d3-41a1-a9bb-3ea2b9e39ca1"), null, false },
                    { new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), new Guid("d0173878-6397-4905-980b-865b3d609f70"), null, false },
                    { new Guid("e0f0044c-8f6e-4324-946c-77c76fa4cec3"), new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), null, false },
                    { new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), new Guid("5c0e4d04-7e42-4448-995d-24c351a31cd1"), null, false },
                    { new Guid("e55a6e1a-5e60-4b3c-a350-b7717bea9381"), new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), null, false },
                    { new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), new Guid("64948c57-4c27-4e2a-8bf1-947973ea1015"), null, false },
                    { new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), null, false },
                    { new Guid("201a5ee2-f06a-4c51-bd79-b7dcb206f80a"), new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), null, false },
                    { new Guid("201a5ee2-f06a-4c51-bd79-b7dcb206f80a"), new Guid("c846cbee-50d3-41a1-a9bb-3ea2b9e39ca1"), null, false },
                    { new Guid("201a5ee2-f06a-4c51-bd79-b7dcb206f80a"), new Guid("eaf089bf-e0ba-42b5-847a-44e57b1629c0"), null, false },
                    { new Guid("201a5ee2-f06a-4c51-bd79-b7dcb206f80a"), new Guid("5c0e4d04-7e42-4448-995d-24c351a31cd1"), null, false },
                    { new Guid("201a5ee2-f06a-4c51-bd79-b7dcb206f80a"), new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), null, false },
                    { new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), new Guid("e460a02c-437b-401d-8fa4-db824f956440"), null, false },
                    { new Guid("60f207de-d14f-4797-a995-2b6ce559eaa4"), new Guid("64948c57-4c27-4e2a-8bf1-947973ea1015"), null, false },
                    { new Guid("60f207de-d14f-4797-a995-2b6ce559eaa4"), new Guid("d0173878-6397-4905-980b-865b3d609f70"), null, false },
                    { new Guid("6a1297e5-a8c6-4d8a-bd9e-1d4b2fef7ebc"), new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), null, false },
                    { new Guid("6a1297e5-a8c6-4d8a-bd9e-1d4b2fef7ebc"), new Guid("e460a02c-437b-401d-8fa4-db824f956440"), null, false },
                    { new Guid("6a1297e5-a8c6-4d8a-bd9e-1d4b2fef7ebc"), new Guid("c2293edb-e8dc-4c66-a8a8-edf82c171dc0"), null, false },
                    { new Guid("6a1297e5-a8c6-4d8a-bd9e-1d4b2fef7ebc"), new Guid("d0173878-6397-4905-980b-865b3d609f70"), null, false },
                    { new Guid("e0f0044c-8f6e-4324-946c-77c76fa4cec3"), new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), null, false },
                    { new Guid("e0f0044c-8f6e-4324-946c-77c76fa4cec3"), new Guid("5c0e4d04-7e42-4448-995d-24c351a31cd1"), null, false },
                    { new Guid("60f207de-d14f-4797-a995-2b6ce559eaa4"), new Guid("0f241bf3-fb2d-4ce0-a9f4-25de3d67b59f"), null, false },
                    { new Guid("e0f0044c-8f6e-4324-946c-77c76fa4cec3"), new Guid("c846cbee-50d3-41a1-a9bb-3ea2b9e39ca1"), null, false }
                });

            migrationBuilder.InsertData(
                table: "CocktailIngredients",
                columns: new[] { "IngredientId", "CocktailId", "DeletedOn", "IsDeleted" },
                values: new object[,]
                {
                    { new Guid("726645d6-8628-4d43-9c9e-7a47b6fc3263"), new Guid("516e12b9-cbee-4ae7-a408-2ad3825f5d28"), null, false },
                    { new Guid("726645d6-8628-4d43-9c9e-7a47b6fc3263"), new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), null, false },
                    { new Guid("726645d6-8628-4d43-9c9e-7a47b6fc3263"), new Guid("201a5ee2-f06a-4c51-bd79-b7dcb206f80a"), null, false },
                    { new Guid("87112b08-16ba-49fb-a2c6-0450cd44b42d"), new Guid("201a5ee2-f06a-4c51-bd79-b7dcb206f80a"), null, false },
                    { new Guid("0bd60dfe-102a-46e0-be63-30741582d6f7"), new Guid("60f207de-d14f-4797-a995-2b6ce559eaa4"), null, false },
                    { new Guid("9b4d2e3a-90ac-47d7-b314-5bcfe9955b8b"), new Guid("6a1297e5-a8c6-4d8a-bd9e-1d4b2fef7ebc"), null, false },
                    { new Guid("857fa6be-8ac4-4442-8db1-9f9570c93458"), new Guid("6a1297e5-a8c6-4d8a-bd9e-1d4b2fef7ebc"), null, false },
                    { new Guid("9b4d2e3a-90ac-47d7-b314-5bcfe9955b8b"), new Guid("516e12b9-cbee-4ae7-a408-2ad3825f5d28"), null, false },
                    { new Guid("9b4d2e3a-90ac-47d7-b314-5bcfe9955b8b"), new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), null, false },
                    { new Guid("84b72c0f-5ef0-448f-a4d0-f1973f8a23e3"), new Guid("4991f748-572f-4ecd-94d2-109d9ab13583"), null, false },
                    { new Guid("84b72c0f-5ef0-448f-a4d0-f1973f8a23e3"), new Guid("e0f0044c-8f6e-4324-946c-77c76fa4cec3"), null, false },
                    { new Guid("1bf3150a-41db-44fb-85aa-c118fab86007"), new Guid("69d0e4d0-4a73-4bd9-87de-c98418f28933"), null, false },
                    { new Guid("5c3b98d2-af05-47ab-9f0f-80788ab0cc47"), new Guid("6a1297e5-a8c6-4d8a-bd9e-1d4b2fef7ebc"), null, false },
                    { new Guid("1bf3150a-41db-44fb-85aa-c118fab86007"), new Guid("bd11ea02-1179-410e-9728-479732c2ffe3"), null, false },
                    { new Guid("419d7e92-65a6-451d-8d05-ed39b19702c1"), new Guid("e6158613-4628-4d9b-8107-7046217e064d"), null, false },
                    { new Guid("08765ef5-2763-419c-b660-29e4d5fcc469"), new Guid("4991f748-572f-4ecd-94d2-109d9ab13583"), null, false },
                    { new Guid("08765ef5-2763-419c-b660-29e4d5fcc469"), new Guid("516e12b9-cbee-4ae7-a408-2ad3825f5d28"), null, false },
                    { new Guid("3621efae-0cba-41fc-8fd6-19b5c2b5de2f"), new Guid("60f207de-d14f-4797-a995-2b6ce559eaa4"), null, false },
                    { new Guid("3621efae-0cba-41fc-8fd6-19b5c2b5de2f"), new Guid("c88b565b-c9dd-44e8-9e84-a7158a796f8f"), null, false },
                    { new Guid("3621efae-0cba-41fc-8fd6-19b5c2b5de2f"), new Guid("516e12b9-cbee-4ae7-a408-2ad3825f5d28"), null, false },
                    { new Guid("17e39e62-2fd6-47cd-8270-a8eaef44c42a"), new Guid("c88b565b-c9dd-44e8-9e84-a7158a796f8f"), null, false },
                    { new Guid("8e64f7ee-85c7-4324-a59c-74daaad9c0b8"), new Guid("c88b565b-c9dd-44e8-9e84-a7158a796f8f"), null, false },
                    { new Guid("8e800e32-7d9f-4c7b-987e-dffe362ab8d7"), new Guid("8dc7b3dc-2577-4f88-8e7a-ff698a9d8c88"), null, false },
                    { new Guid("a51c386e-57aa-48f7-aad9-626dfc641c0e"), new Guid("8dc7b3dc-2577-4f88-8e7a-ff698a9d8c88"), null, false },
                    { new Guid("471d21df-564e-4697-8c32-e5a97e053444"), new Guid("bd11ea02-1179-410e-9728-479732c2ffe3"), null, false },
                    { new Guid("471d21df-564e-4697-8c32-e5a97e053444"), new Guid("24647136-f565-46a2-abc8-0a80e7e34f07"), null, false },
                    { new Guid("9fd5730a-d9c5-4f0b-9c01-9a9966070e08"), new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), null, false },
                    { new Guid("6aa0f5e0-942e-4dd3-873c-50e6ef71120a"), new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), null, false },
                    { new Guid("9ceda011-ee58-4aad-9d3a-95b82dd03817"), new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), null, false },
                    { new Guid("e85bacd8-d038-42cd-a66d-29c7da1450f1"), new Guid("69d0e4d0-4a73-4bd9-87de-c98418f28933"), null, false },
                    { new Guid("8e800e32-7d9f-4c7b-987e-dffe362ab8d7"), new Guid("e55a6e1a-5e60-4b3c-a350-b7717bea9381"), null, false },
                    { new Guid("6e6d43ca-1378-4165-b882-fbf28cd0fcde"), new Guid("c4d90efc-a88a-40a5-a103-75e5bc2adafd"), null, false },
                    { new Guid("e26e3bcb-c810-4196-b0bf-55c8c824acbb"), new Guid("60f207de-d14f-4797-a995-2b6ce559eaa4"), null, false },
                    { new Guid("519495da-2ca5-41a2-b361-6ec190d43692"), new Guid("c1f9f9e9-108e-452b-a2a3-aeefd363a7eb"), null, false },
                    { new Guid("519495da-2ca5-41a2-b361-6ec190d43692"), new Guid("e0f0044c-8f6e-4324-946c-77c76fa4cec3"), null, false },
                    { new Guid("a3b05b48-6c99-4f8f-879b-e521dbca5322"), new Guid("c1f9f9e9-108e-452b-a2a3-aeefd363a7eb"), null, false },
                    { new Guid("a3b05b48-6c99-4f8f-879b-e521dbca5322"), new Guid("e0f0044c-8f6e-4324-946c-77c76fa4cec3"), null, false },
                    { new Guid("52675120-68a9-4481-8fd5-32ef028cffbc"), new Guid("c1f9f9e9-108e-452b-a2a3-aeefd363a7eb"), null, false },
                    { new Guid("52675120-68a9-4481-8fd5-32ef028cffbc"), new Guid("c4d90efc-a88a-40a5-a103-75e5bc2adafd"), null, false },
                    { new Guid("ca055b38-7d93-436d-904a-bc94c3f7cbd7"), new Guid("c1f9f9e9-108e-452b-a2a3-aeefd363a7eb"), null, false },
                    { new Guid("ca055b38-7d93-436d-904a-bc94c3f7cbd7"), new Guid("c4d90efc-a88a-40a5-a103-75e5bc2adafd"), null, false },
                    { new Guid("ca055b38-7d93-436d-904a-bc94c3f7cbd7"), new Guid("6a1297e5-a8c6-4d8a-bd9e-1d4b2fef7ebc"), null, false },
                    { new Guid("54ff08c2-f064-47a2-bdde-cf3bd955d867"), new Guid("c1f9f9e9-108e-452b-a2a3-aeefd363a7eb"), null, false },
                    { new Guid("54ff08c2-f064-47a2-bdde-cf3bd955d867"), new Guid("8dc7b3dc-2577-4f88-8e7a-ff698a9d8c88"), null, false },
                    { new Guid("672fdcd1-a606-42e8-8ce1-397a92379704"), new Guid("c4d90efc-a88a-40a5-a103-75e5bc2adafd"), null, false },
                    { new Guid("672fdcd1-a606-42e8-8ce1-397a92379704"), new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), null, false },
                    { new Guid("672fdcd1-a606-42e8-8ce1-397a92379704"), new Guid("69d0e4d0-4a73-4bd9-87de-c98418f28933"), null, false },
                    { new Guid("672fdcd1-a606-42e8-8ce1-397a92379704"), new Guid("e6158613-4628-4d9b-8107-7046217e064d"), null, false },
                    { new Guid("672fdcd1-a606-42e8-8ce1-397a92379704"), new Guid("516e12b9-cbee-4ae7-a408-2ad3825f5d28"), null, false },
                    { new Guid("130807eb-01a3-47bc-a4b6-a66d8ee5502b"), new Guid("c4d90efc-a88a-40a5-a103-75e5bc2adafd"), null, false },
                    { new Guid("130807eb-01a3-47bc-a4b6-a66d8ee5502b"), new Guid("e55a6e1a-5e60-4b3c-a350-b7717bea9381"), null, false },
                    { new Guid("130807eb-01a3-47bc-a4b6-a66d8ee5502b"), new Guid("6a1297e5-a8c6-4d8a-bd9e-1d4b2fef7ebc"), null, false },
                    { new Guid("130807eb-01a3-47bc-a4b6-a66d8ee5502b"), new Guid("60f207de-d14f-4797-a995-2b6ce559eaa4"), null, false },
                    { new Guid("130807eb-01a3-47bc-a4b6-a66d8ee5502b"), new Guid("cb78fcf6-a021-4efb-b4c0-54af9218601e"), null, false },
                    { new Guid("130807eb-01a3-47bc-a4b6-a66d8ee5502b"), new Guid("e6158613-4628-4d9b-8107-7046217e064d"), null, false },
                    { new Guid("d7a995af-c9e9-4fe2-843c-851314248b6c"), new Guid("e9371fec-6941-47f3-ab96-3a2e25d9ecef"), null, false },
                    { new Guid("d7a995af-c9e9-4fe2-843c-851314248b6c"), new Guid("e55a6e1a-5e60-4b3c-a350-b7717bea9381"), null, false },
                    { new Guid("d7a995af-c9e9-4fe2-843c-851314248b6c"), new Guid("8dc7b3dc-2577-4f88-8e7a-ff698a9d8c88"), null, false },
                    { new Guid("d7a995af-c9e9-4fe2-843c-851314248b6c"), new Guid("bd11ea02-1179-410e-9728-479732c2ffe3"), null, false },
                    { new Guid("d7a995af-c9e9-4fe2-843c-851314248b6c"), new Guid("24647136-f565-46a2-abc8-0a80e7e34f07"), null, false },
                    { new Guid("d7a995af-c9e9-4fe2-843c-851314248b6c"), new Guid("516e12b9-cbee-4ae7-a408-2ad3825f5d28"), null, false },
                    { new Guid("e26e3bcb-c810-4196-b0bf-55c8c824acbb"), new Guid("e55a6e1a-5e60-4b3c-a350-b7717bea9381"), null, false },
                    { new Guid("e26e3bcb-c810-4196-b0bf-55c8c824acbb"), new Guid("516e12b9-cbee-4ae7-a408-2ad3825f5d28"), null, false },
                    { new Guid("672fdcd1-a606-42e8-8ce1-397a92379704"), new Guid("6a1297e5-a8c6-4d8a-bd9e-1d4b2fef7ebc"), null, false }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_BarCocktails_BarId",
                table: "BarCocktails",
                column: "BarId");

            migrationBuilder.CreateIndex(
                name: "IX_BarComments_BarId",
                table: "BarComments",
                column: "BarId");

            migrationBuilder.CreateIndex(
                name: "IX_BarComments_UserId",
                table: "BarComments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_BarRatings_UserId",
                table: "BarRatings",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_CocktailComments_CocktailId",
                table: "CocktailComments",
                column: "CocktailId");

            migrationBuilder.CreateIndex(
                name: "IX_CocktailComments_UserId",
                table: "CocktailComments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_CocktailIngredients_CocktailId",
                table: "CocktailIngredients",
                column: "CocktailId");

            migrationBuilder.CreateIndex(
                name: "IX_CocktailRatings_UserId",
                table: "CocktailRatings",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "BarCocktails");

            migrationBuilder.DropTable(
                name: "BarComments");

            migrationBuilder.DropTable(
                name: "BarRatings");

            migrationBuilder.DropTable(
                name: "CocktailComments");

            migrationBuilder.DropTable(
                name: "CocktailIngredients");

            migrationBuilder.DropTable(
                name: "CocktailRatings");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Bars");

            migrationBuilder.DropTable(
                name: "Ingredients");

            migrationBuilder.DropTable(
                name: "Cocktails");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
