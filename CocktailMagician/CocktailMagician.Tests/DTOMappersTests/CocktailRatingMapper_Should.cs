﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers;
using CocktailMagician.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.DTOMappersTests
{
    [TestClass]
    public class CocktailRatingMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var rating = new CocktailRating
            {
                CocktailId = cocktail.Id,
                Cocktail = cocktail,
                UserId = user.Id,
                User = user,
                Rating = 5
            };

            var sut = new CocktailRatingMapper();

            var result = sut.MapToDTO(rating);

            Assert.IsInstanceOfType(result, typeof(CocktailRatingDTO));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailRating_ToDTO()
        {
            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var rating = new CocktailRating
            {
                CocktailId = cocktail.Id,
                Cocktail = cocktail,
                UserId = user.Id,
                User = user,
                Rating = 5
            };

            var sut = new CocktailRatingMapper();

            var result = sut.MapToDTO(rating);

            Assert.AreEqual(rating.UserId, result.UserId);
            Assert.AreEqual(rating.CocktailId, result.CocktailId);
            Assert.AreEqual(rating.Rating, result.Rating);
        }

        [TestMethod]
        public void ReturnCorrectInstance_OfCollection()
        {
            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician2@cm.com"
            };

            var ratings = new List<CocktailRating>()
            {
                new CocktailRating
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail,
                    UserId = user.Id,
                    User = user,
                    Rating = 5
                },

                new CocktailRating
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail,
                    UserId = user2.Id,
                    User = user2,
                    Rating = 4
                }
            };

            var sut = new CocktailRatingMapper();

            var result = sut.MapToDTO(ratings);

            Assert.IsInstanceOfType(result, typeof(ICollection<CocktailRatingDTO>));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailRating_ToDTO_Collection()
        {
            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician2@cm.com"
            };

            var ratings = new List<CocktailRating>()
            {
                new CocktailRating
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail,
                    UserId = user.Id,
                    User = user,
                    Rating = 5
                },

                new CocktailRating
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail,
                    UserId = user2.Id,
                    User = user2,
                    Rating = 4
                }
            };

            var sut = new CocktailRatingMapper();

            var result = sut.MapToDTO(ratings);
            var resultList = result.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(ratings[i].UserId, resultList[i].UserId);
                Assert.AreEqual(ratings[i].CocktailId, resultList[i].CocktailId);
                Assert.AreEqual(ratings[i].Rating, resultList[i].Rating);
            }
            Assert.AreEqual(2, resultList.Count);
        }
    }
}
