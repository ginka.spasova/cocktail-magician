﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers;
using CocktailMagician.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.DTOMappersTests
{
    [TestClass]
    public class IngredientMapper_Should
    {
        [TestMethod]
        public void Return_CorrectInstance()
        {
            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var sut = new IngredientMapper();

            var result = sut.MapToDTO(ingredient);

            Assert.IsInstanceOfType(result, typeof(IngredientDTO));
        }

        [TestMethod]
        public void MapCorrectly_FromIngredient_ToDTO()
        {
            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var sut = new IngredientMapper();

            var result = sut.MapToDTO(ingredient);

            Assert.AreEqual(ingredient.Id, result.Id);
            Assert.AreEqual(ingredient.Name, result.Name);
        }

        [TestMethod]
        public void ReturnCorrectInstance_Of_Collection()
        {
            var ingredients = new List<Ingredient>()
            {
                new Ingredient
                {
                    Id = Guid.NewGuid(),
                    Name = "Wine"
                },

                new Ingredient
                {
                    Id = Guid.NewGuid(),
                    Name = "Blueberry",
                }
            };
            
            var sut = new IngredientMapper();

            var result = sut.MapToDTO(ingredients);

            Assert.IsInstanceOfType(result, typeof(ICollection<IngredientDTO>));
        }

        [TestMethod]
        public void MapCorrectly_From_Ingredient_ToDTO_Collection()
        {
            var ingredients = new List<Ingredient>()
            {
                new Ingredient
                {
                    Id = Guid.NewGuid(),
                    Name = "Wine"
                },

                new Ingredient
                {
                    Id = Guid.NewGuid(),
                    Name = "Blueberry",
                }
            };

            var sut = new IngredientMapper();

            var result = sut.MapToDTO(ingredients);
            var resultList = result.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(ingredients[i].Id, resultList[i].Id);
                Assert.AreEqual(ingredients[i].Name, resultList[i].Name);
            }
        }
    }
}
