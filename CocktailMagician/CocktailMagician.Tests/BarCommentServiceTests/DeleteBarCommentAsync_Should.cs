﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.BarCommentServiceTests
{
    [TestClass]
    public class DeleteBarCommentAsync_Should
    {
        [TestClass]
        public class EditBarCommentAsync_Should
        {
            [TestMethod]
            public async Task ReturnTrue_When_ValidParams()
            {
                var options = Utilities.GetOptions(nameof(ReturnTrue_When_ValidParams));

                var barId = Guid.NewGuid();
                var userId = Guid.NewGuid();

                var barComment = new BarComment
                {
                    Id = Guid.NewGuid(),
                    BarId = barId,
                    UserId = userId,
                    Text = "cool bar"
                };

                var barCommentDTO = new BarCommentDTO
                {
                    Id = barComment.Id,
                    BarId = barComment.BarId,
                    UserId = barComment.UserId,
                    User = "someuser",
                    Text = barComment.Text
                };

                var mockMapper = new Mock<IDTOMapper<BarComment, BarCommentDTO>>();
                var mockFactory = new Mock<IFactory>();

                mockMapper.Setup(x => x.MapToDTO(It.IsAny<BarComment>())).Returns(barCommentDTO);

                using (var arrangeContext = new CMDbContext(options))
                {
                    await arrangeContext.BarComments.AddAsync(barComment);
                    await arrangeContext.SaveChangesAsync();
                }

                var id = barComment.Id;

                using (var assertContext = new CMDbContext(options))
                {
                    //Act & Assert
                    var sut = new BarCommentService(assertContext, mockMapper.Object, mockFactory.Object);

                    var result = await sut.DeleteBarCommentAsync(id);

                    Assert.IsTrue(result);
                }
            }

            [TestMethod]
            public async Task ReturnFalse_When_CommentNotFound()
            {
                var options = Utilities.GetOptions(nameof(ReturnFalse_When_CommentNotFound));

                var barId = Guid.NewGuid();
                var userId = Guid.NewGuid();

                var barComment = new BarComment
                {
                    Id = Guid.NewGuid(),
                    BarId = barId,
                    UserId = userId,
                    Text = "cool bar"
                };

                var barCommentDTO = new BarCommentDTO
                {
                    Id = barComment.Id,
                    BarId = barComment.BarId,
                    UserId = barComment.UserId,
                    User = "someuser",
                    Text = barComment.Text
                };

                var mockMapper = new Mock<IDTOMapper<BarComment, BarCommentDTO>>();
                var mockFactory = new Mock<IFactory>();

                mockMapper.Setup(x => x.MapToDTO(It.IsAny<BarComment>())).Returns(barCommentDTO);

                var id = barComment.Id;

                using (var assertContext = new CMDbContext(options))
                {
                    //Act & Assert
                    var sut = new BarCommentService(assertContext, mockMapper.Object, mockFactory.Object);

                    var result = await sut.DeleteBarCommentAsync(id);

                    Assert.IsFalse(result);
                }
            }
        }
    }
}
