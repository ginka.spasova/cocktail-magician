﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.BarCommentServiceTests
{
    [TestClass]
    public class CreateBarCommentAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBarComment()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBarComment));

            var barId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var barComment = new BarComment
            {
                Id = Guid.NewGuid(),
                BarId = barId,
                UserId = userId,
                Text = "cool bar"
            };

            var barCommentDTO = new BarCommentDTO
            {
                Id = barComment.Id,
                BarId = barComment.BarId,
                UserId = barComment.UserId,
                User = "someuser",
                Text = barComment.Text
            };

            var mockMapper = new Mock<IDTOMapper<BarComment, BarCommentDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockFactory.Setup(fact => fact.CreateBarComment(It.IsAny<string>()))
                       .Returns(barComment);

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<BarComment>())).Returns(barCommentDTO);

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarCommentService(assertContext, mockMapper.Object, mockFactory.Object);

                var result = await sut.CreateBarCommentAsync(barCommentDTO);

                Assert.IsInstanceOfType(result, typeof(BarCommentDTO));
                Assert.AreEqual(barCommentDTO.Id, result.Id);
                Assert.AreEqual(barCommentDTO.Text, result.Text);
                Assert.AreEqual(barCommentDTO.UserId, result.UserId);
                Assert.AreEqual(barCommentDTO.BarId, result.BarId);
            }
        }

        [TestMethod]
        public async Task Throw_When_BarCommentDTO_IsNull()
        {
            var options = Utilities.GetOptions(nameof(Throw_When_BarCommentDTO_IsNull));

            var mockMapper = new Mock<IDTOMapper<BarComment, BarCommentDTO>>();
            var mockFactory = new Mock<IFactory>();

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarCommentService(assertContext, mockMapper.Object, mockFactory.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateBarCommentAsync(null));
            }
        }
    }
}
