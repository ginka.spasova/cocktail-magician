﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailRatingServiceTests
{
    [TestClass]
   public class GetAllCocktailRatingsAsync_Should
    {
        [TestMethod]

        public async Task ReturnsAllCocktailRatings()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnsAllCocktailRatings));

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "TestUser",

            };
            var user2 = new User
            {
                Id = Guid.NewGuid(),
                UserName = "TestUser2",

            };


            var cocktailRating = new CocktailRating 
            {
                Rating = 4,
                CocktailId = cocktail.Id,
                UserId = user.Id,
            };
            var cocktailRating2 = new CocktailRating
            {
                Rating = 3,
                CocktailId = cocktail.Id,
                UserId = user2.Id,
            };

            var cocktailRatingDTO = new CocktailRatingDTO
            {
                CocktailId = cocktail.Id,
                UserId = user.Id,
                Rating = 4
            };

            var cocktailRatingDTO2 = new CocktailRatingDTO
            {
                CocktailId = cocktail.Id,
                UserId = user2.Id,
                Rating = 3
            };
            var cocktailRatingDTOs = new List<CocktailRatingDTO> { cocktailRatingDTO, cocktailRatingDTO2 };

            var mockMapper = new Mock<IDTOMapper<CocktailRating, CocktailRatingDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<ICollection<CocktailRating>>()))
                .Returns(cocktailRatingDTOs);

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Users.AddAsync(user2);
                await arrangeContext.CocktailRatings.AddAsync(cocktailRating);
                await arrangeContext.CocktailRatings.AddAsync(cocktailRating2);
                await arrangeContext.SaveChangesAsync();
            }
            //Act&Assert
            using(var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailRatingService(assertContext, mockFactory.Object, mockMapper.Object);

                var result = await sut.GetAllCocktailRatingsAsync(cocktail.Id);
                var resultList = result.ToList();

                var dbCocktailRatings = await assertContext.CocktailRatings
                    .Where(cr => cr.CocktailId == cocktail.Id).ToListAsync();

                for (int i = 0; i < dbCocktailRatings.Count; i++)
                {
                    Assert.AreEqual(cocktailRatingDTOs[i].UserId, dbCocktailRatings[i].UserId);
                    Assert.AreEqual(cocktailRatingDTOs[i].Rating, dbCocktailRatings[i].Rating);
                }
                Assert.AreEqual(cocktailRatingDTOs.Count, dbCocktailRatings.Count);
                    
            }
        }
    }
}
