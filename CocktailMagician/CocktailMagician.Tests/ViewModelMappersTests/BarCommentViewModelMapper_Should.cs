﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Areas.Admin.Models;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.Models;
using CocktailMagician.Web.ViewModelMappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.ViewModelMappersTests
{
    [TestClass]
    public class BarCommentViewModelMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var bar = new BarDTO
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var comment = new BarCommentDTO
            {
                BarId = bar.Id,
                Bar = bar.Name,
                UserId = user.Id,
                User = user.Name,
                Text = "I like it"
            };

            var sut = new BarCommentViewModelMapper();

            var result = sut.MapToViewModel(comment);

            Assert.IsInstanceOfType(result, typeof(BarCommentViewModel));
        }

        [TestMethod]
        public void MapCorrectly_From_BarCommentDTO_ToViewModel()
        {
            var bar = new BarDTO
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var comment = new BarCommentDTO
            {
                BarId = bar.Id,
                Bar = bar.Name,
                UserId = user.Id,
                User = user.Name,
                Text = "I like it"
            };

            var sut = new BarCommentViewModelMapper();

            var result = sut.MapToViewModel(comment);

            Assert.AreEqual(comment.UserId, result.UserId);
            Assert.AreEqual(comment.BarId, result.BarId);
            Assert.AreEqual(comment.Text, result.Text);
        }

        [TestMethod]
        public void ReturnCorrectInstance_Of_Collection()
        {
            var bar = new BarDTO
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var user2 = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician2@cm.com"
            };

            var comments = new List<BarCommentDTO>()
            {
                new BarCommentDTO
                {
                    BarId = bar.Id,
                    Bar = bar.Name,
                    UserId = user.Id,
                    User = user.Name,
                    Text = "I like it"
                },

                new BarCommentDTO
                {
                    BarId = bar.Id,
                    Bar = bar.Name,
                    UserId = user2.Id,
                    User = user2.Name,
                    Text = "It was nice"
                }
            };

            var sut = new BarCommentViewModelMapper();

            var result = sut.MapToViewModel(comments);

            Assert.IsInstanceOfType(result, typeof(ICollection<BarCommentViewModel>));
        }

        [TestMethod]
        public void MapCorrectly_From_BarCommentDTO_ToViewModel_Collection()
        {
            var bar = new BarDTO
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var user2 = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician2@cm.com"
            };

            var comments = new List<BarCommentDTO>()
            {
                new BarCommentDTO
                {
                    BarId = bar.Id,
                    Bar = bar.Name,
                    UserId = user.Id,
                    User = user.Name,
                    Text = "I like it"
                },

                new BarCommentDTO
                {
                    BarId = bar.Id,
                    Bar = bar.Name,
                    UserId = user2.Id,
                    User = user2.Name,
                    Text = "It was nice"
                }
            };

            var sut = new BarCommentViewModelMapper();

            var result = sut.MapToViewModel(comments);
            var resultList = result.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(comments[i].UserId, resultList[i].UserId);
                Assert.AreEqual(comments[i].BarId, resultList[i].BarId);
                Assert.AreEqual(comments[i].Text, resultList[i].Text);
            }
            Assert.AreEqual(2, resultList.Count);
        }

        [TestMethod]
        public void Return_CorrectInstance()
        {
            var bar = new BarViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new UserViewModel
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var comment = new BarCommentViewModel
            {
                BarId = bar.Id,
                UserId = user.Id,
                Text = "I like it"
            };

            var sut = new BarCommentViewModelMapper();

            var result = sut.MapToDTO(comment);

            Assert.IsInstanceOfType(result, typeof(BarCommentDTO));
        }

        [TestMethod]
        public void MapCorrectly_From_BarCommentViewModel_ToDTO()
        {
            var bar = new BarViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new UserViewModel
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var comment = new BarCommentViewModel
            {
                BarId = bar.Id,
                UserId = user.Id,
                Text = "I like it"
            };

            var sut = new BarCommentViewModelMapper();

            var result = sut.MapToDTO(comment);

            Assert.AreEqual(comment.UserId, result.UserId);
            Assert.AreEqual(comment.BarId, result.BarId);
            Assert.AreEqual(comment.Text, result.Text);
        }
    }
}
