﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Models;
using CocktailMagician.Web.ViewModelMappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.ViewModelMappersTests
{
    [TestClass]
    public class BarViewModelMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var bar = new BarDTO
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath",
                Cocktails = new List<CocktailDTO>
                {
                    new CocktailDTO
                    {
                        Id = Guid.NewGuid(),
                        Name = "Martini",
                        ImgPath = "testpath"
                    }
                },
                
            };

            var sut = new BarViewModelMapper();

            var result = sut.MapToViewModel(bar);

            Assert.IsInstanceOfType(result, typeof(BarViewModel));
        }

        [TestMethod]
        public void CorrectlyMap_From_BarDTO_ToViewModel()
        {
            var bar = new BarDTO
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath",
                Cocktails = new List<CocktailDTO>
                {
                    new CocktailDTO
                    {
                        Id = Guid.NewGuid(),
                        Name = "Martini",
                        ImgPath = "testpath"
                    }
                }
            };

            var sut = new BarViewModelMapper();

            var result = sut.MapToViewModel(bar);

            Assert.AreEqual(result.Id, bar.Id);
            Assert.AreEqual(result.Name, bar.Name);
            Assert.AreEqual(result.Description, bar.Description);
            Assert.AreEqual(result.Address, bar.Address);
            Assert.AreEqual(result.Phone, bar.Phone);
            Assert.AreEqual(result.ImgPath, bar.ImgPath);
        }

        [TestMethod]
        public void Return_CorrectInstanceOfCollection()
        {
            var bars = new List<BarDTO>()
            {
                new BarDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "Terminal 1",
                    Description = "Very cool",
                    Phone = "111-222-333",
                    Address = "Sofia, Angel Kanchev 1",
                    Cocktails = new List<CocktailDTO>
                    {
                        new CocktailDTO
                        {
                            Id = Guid.NewGuid(),
                            Name = "Martini",
                            ImgPath = "testpath"
                        }
                    }
                },

                new BarDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "RockIt",
                    Description = "So nice",
                    Phone = "111-555-333",
                    Address = "Sofia, Petko Karavelov 5",
                    Cocktails = new List<CocktailDTO>
                    {
                        new CocktailDTO
                        {
                            Id = Guid.NewGuid(),
                            Name = "Pina colada",
                            ImgPath = "testpath"
                        }
                    }
                }
            };

            var sut = new BarViewModelMapper();

            var result = sut.MapToViewModel(bars);

            Assert.IsInstanceOfType(result, typeof(ICollection<BarViewModel>));
        }
        [TestMethod]
        public void CorrectlyMapFrom_BarDTO_ToViewModel_Collection()
        {
            var bars = new List<BarDTO>()
            {
                new BarDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "Terminal 1",
                    Description = "Very cool",
                    Phone = "111-222-333",
                    Address = "Sofia, Angel Kanchev 1",
                    Cocktails = new List<CocktailDTO>
                    {
                        new CocktailDTO
                        {
                            Id = Guid.NewGuid(),
                            Name = "Martini",
                            ImgPath = "testpath"
                        }
                    }
                },

                new BarDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "RockIt",
                    Description = "So nice",
                    Phone = "111-555-333",
                    Address = "Sofia, Petko Karavelov 5",
                    Cocktails = new List<CocktailDTO>
                    {
                        new CocktailDTO
                        {
                            Id = Guid.NewGuid(),
                            Name = "Pina colada",
                            ImgPath = "testpath"
                        }
                    }
                }
            };

            var sut = new BarViewModelMapper();

            var result = sut.MapToViewModel(bars);
            var resultList = result.ToList();
            
            Assert.AreEqual(2, result.Count);
            
            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(resultList[i].Id, bars[i].Id);
                Assert.AreEqual(resultList[i].Name, bars[i].Name);
                Assert.AreEqual(resultList[i].Description, bars[i].Description);
                Assert.AreEqual(resultList[i].Address, bars[i].Address);
                Assert.AreEqual(resultList[i].Phone, bars[i].Phone);
                Assert.AreEqual(resultList[i].ImgPath, bars[i].ImgPath);
            }
        }

        [TestMethod]
        public void Return_CorrectInstance()
        {
            var bar = new BarViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath",
                Cocktails = new List<CocktailViewModel>
                {
                    new CocktailViewModel
                    {
                        Id = Guid.NewGuid(),
                        Name = "Martini",
                        ImgPath = "testpath"
                    }
                }
            };

            var sut = new BarViewModelMapper();

            var result = sut.MapToDTO(bar);

            Assert.IsInstanceOfType(result, typeof(BarDTO));
        }

        [TestMethod]
        public void CorrectlyMap_From_BarViewModel_ToDTO()
        {
            var bar = new BarViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath",
                Cocktails = new List<CocktailViewModel>
                {
                    new CocktailViewModel
                    {
                        Id = Guid.NewGuid(),
                        Name = "Martini",
                        ImgPath = "testpath"
                    }
                }
            };

            var sut = new BarViewModelMapper();

            var result = sut.MapToDTO(bar);

            Assert.AreEqual(result.Id, bar.Id);
            Assert.AreEqual(result.Name, bar.Name);
            Assert.AreEqual(result.Description, bar.Description);
            Assert.AreEqual(result.Address, bar.Address);
            Assert.AreEqual(result.Phone, bar.Phone);
            Assert.AreEqual(result.ImgPath, bar.ImgPath);
        }
    }
}
