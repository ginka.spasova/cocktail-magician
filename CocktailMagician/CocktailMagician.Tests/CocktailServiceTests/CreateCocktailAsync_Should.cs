﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailServiceTests
{
    [TestClass]
    public class CreateCocktailAsync_Should
    {
        [TestMethod]

        public async Task CreatesNewCocktail()
        {
            var options = Utilities.GetOptions(nameof(CreatesNewCocktail));

            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "TestIngredientOne",
            };

            var ingredient2 = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "TestIngredientTwo",
            };

            var ingredientDTO = new IngredientDTO { Id = ingredient.Id, Name = ingredient.Name };
            var ingredientDTO2 = new IngredientDTO { Id = ingredient2.Id, Name = ingredient2.Name };

            var cocktailDTO = new CocktailDTO
            {
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath",
                Ingredients = new List<IngredientDTO> { ingredientDTO, ingredientDTO2 },
                Id = Guid.NewGuid()
            };

            var cocktail = new Cocktail
            {
                Name = cocktailDTO.Name,
                Description = cocktailDTO.Description,
                ImgPath = cocktailDTO.ImgPath,
                Id = cocktailDTO.Id
            };

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockFactory.Setup(x => x.CreateCocktail(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(cocktail);


            mockFactory.SetupSequence(x => x.CreateCocktailIngredient(It.IsAny<Guid>(), It.IsAny<Guid>()))
            .Returns(new CocktailIngredient { CocktailId = cocktailDTO.Id, IngredientId = ingredient.Id })
            .Returns(new CocktailIngredient { CocktailId = cocktailDTO.Id, IngredientId = ingredient2.Id });

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Cocktail>())).Returns(cocktailDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Ingredients.AddAsync(ingredient);
                await arrangeContext.Ingredients.AddAsync(ingredient2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.CreateCocktailAsync(cocktailDTO);
                var dbCocktail =await assertContext.Cocktails.FindAsync(cocktailDTO.Id);
                
                Assert.AreEqual(cocktailDTO.Name, dbCocktail.Name);
                Assert.AreEqual(cocktailDTO.Description, dbCocktail.Description);
                Assert.AreEqual(cocktailDTO.ImgPath, dbCocktail.ImgPath);
                Assert.IsTrue(assertContext.CocktailIngredients.Any(ci => ci.IngredientId == ingredient.Id && ci.CocktailId == cocktailDTO.Id));
                Assert.IsTrue(assertContext.CocktailIngredients.Any(ci => ci.IngredientId == ingredient2.Id && ci.CocktailId == cocktailDTO.Id));
            }
        }
        [TestMethod]

        public async Task ThrowsException_When_AlreadyExists()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowsException_When_AlreadyExists));
            var cocktailDTO = new CocktailDTO
            {
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath",
                Id = Guid.NewGuid()
            };

            var cocktail = new Cocktail
            {
                Name = cocktailDTO.Name,
                Description = cocktailDTO.Description,
                ImgPath = cocktailDTO.ImgPath,
                Id = cocktailDTO.Id
            };

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockFactory.Setup(x => x.CreateCocktail(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(cocktail);

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Cocktail>())).Returns(cocktailDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateCocktailAsync(cocktailDTO));
            }
        }
        [TestMethod]

        public async Task ThrowsNullException_When_CocktailDTOIsNull()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowsNullException_When_CocktailDTOIsNull));

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            CocktailDTO nullCocktailDTO = null;
            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateCocktailAsync(nullCocktailDTO));
            }
        }
    }
}
