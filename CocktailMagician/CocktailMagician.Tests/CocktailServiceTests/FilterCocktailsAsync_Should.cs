﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailServiceTests
{
    [TestClass]
    public class FilterCocktailsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCocktailsFilteredByName()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnCocktailsFilteredByName));

            var cocktail1 = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "BTestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };
            var cocktail2 = new Cocktail()
            {

                Id = Guid.NewGuid(),
                Name = "ATestCocktail",
                Description = "TestDescriptionTwo",
                ImgPath = "TestPathTwo"
            };

            var cocktails = new List<Cocktail> { cocktail1, cocktail2 };

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();


            mockMapper.Setup(x => x.MapToDTO(It.IsAny<PaginatedList<Cocktail>>()))
                .Returns<PaginatedList<Cocktail>>(pl => new PageTransferInfo<CocktailDTO>
                {
                    HasNextPage = pl.HasNextPage,
                    HasPreviousPage = pl.HasPreviousPage,
                    TotalPages = pl.TotalPages,
                    PageIndex = pl.PageIndex,
                    Collection = pl.Select(c => new CocktailDTO
                    {
                        Name = c.Name,
                        Id = c.Id,
                        Description = c.Description,
                        ImgPath = c.ImgPath
                    }).ToList()
                });

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail1);
                await arrangeContext.Cocktails.AddAsync(cocktail2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.FilterCocktailsAsync("ATest", 1);
                var resultList = result.Collection.ToList();

                var dbList = await assertContext.Cocktails.Where(c => c.Name.Contains("ATest"))
                    .ToListAsync();

                for (int i = 0; i < dbList.Count; i++)
                {
                    Assert.AreEqual(resultList[i].Name, dbList[i].Name);
                    Assert.AreEqual(resultList[i].Description, dbList[i].Description);
                    Assert.AreEqual(resultList[i].ImgPath, dbList[i].ImgPath);
                    Assert.AreEqual(resultList[i].Id, dbList[i].Id);
                }
                Assert.AreEqual(1, result.Collection.Count);
            }
        }
        [TestMethod]
        public async Task ReturnsCocktailsFilteredByIngredients()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnCocktailsFilteredByName));
            var ingredient = new Ingredient
            {
                Name = "TestIngredient",
                Id = Guid.NewGuid()
            };
        
            var cocktail1 = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "BTestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };
            var cocktail2 = new Cocktail()
            {

                Id = Guid.NewGuid(),
                Name = "ATestCocktail",
                Description = "TestDescriptionTwo",
                ImgPath = "TestPathTwo"
            };
            var cocktailIngredient = new CocktailIngredient { CocktailId = cocktail1.Id, IngredientId = ingredient.Id };
            cocktail1.Ingredients.Add(cocktailIngredient);
            var cocktails = new List<Cocktail> { cocktail1, cocktail2 };

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();


            mockMapper.Setup(x => x.MapToDTO(It.IsAny<PaginatedList<Cocktail>>()))
                .Returns<PaginatedList<Cocktail>>(pl => new PageTransferInfo<CocktailDTO>
                {
                    HasNextPage = pl.HasNextPage,
                    HasPreviousPage = pl.HasPreviousPage,
                    TotalPages = pl.TotalPages,
                    PageIndex = pl.PageIndex,
                    Collection = pl.Select(c => new CocktailDTO
                    {
                        Name = c.Name,
                        Id = c.Id,
                        Description = c.Description,
                        ImgPath = c.ImgPath,
                        Ingredients = c.Ingredients.Select(i => 
                        new IngredientDTO { Name = i.Ingredient.Name, Id = i.IngredientId }).ToList()
                    }).ToList()
                }) ;

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Ingredients.AddAsync(ingredient);
                await arrangeContext.CocktailIngredients.AddAsync(cocktailIngredient);
                await arrangeContext.Cocktails.AddAsync(cocktail1);
                await arrangeContext.Cocktails.AddAsync(cocktail2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.FilterCocktailsAsync("TestIngr", 1);
                var resultList = result.Collection.ToList();

                var dbList = await assertContext.Cocktails
                    .Where(c => c.Ingredients.Any(i => i.Ingredient.Name.Contains("TestIngr")))
                    .ToListAsync();

                for (int i = 0; i < dbList.Count; i++)
                {
                    Assert.AreEqual(resultList[i].Name, dbList[i].Name);
                    Assert.AreEqual(resultList[i].Description, dbList[i].Description);
                    Assert.AreEqual(resultList[i].ImgPath, dbList[i].ImgPath);
                    Assert.AreEqual(resultList[i].Id, dbList[i].Id);
                }
                Assert.AreEqual(1, result.Collection.Count);
            }
        }

    }
}
