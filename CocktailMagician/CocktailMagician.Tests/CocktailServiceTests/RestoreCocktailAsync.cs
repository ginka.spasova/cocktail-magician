﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailServiceTests
{
    [TestClass]
    public class RestoreCocktailAsync
    {
        [TestMethod]
        public async Task ReturnTrue_And_RestoreCocktail()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnTrue_And_RestoreCocktail));

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath",
                IsDeleted = true
            };

            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "TestIngredientOne",
            };

            var ingredient2 = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "TestIngredientTwo",
            };
            var cocktailIngredient = new CocktailIngredient { CocktailId = cocktail.Id, IngredientId = ingredient.Id, IsDeleted = true };
            var cocktailIngredient2 = new CocktailIngredient { CocktailId = cocktail.Id, IngredientId = ingredient2.Id, IsDeleted = true };
            var cocktailIngredients = new List<CocktailIngredient> { cocktailIngredient, cocktailIngredient2 };

            cocktail.Ingredients = cocktailIngredients;
            var id = cocktail.Id;
            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Ingredients.AddAsync(ingredient);
                await arrangeContext.Ingredients.AddAsync(ingredient2);
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.RestoreCocktailAsync(id);

                Assert.IsTrue(result);

                Assert.IsTrue(assertContext.Cocktails
                   .Any(c => c.Id == cocktail.Id && c.IsDeleted == false));
            }
        }
        [TestMethod]
        public async Task ReturnFalse_When_NoDeletedCocktail()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnFalse_When_NoDeletedCocktail));

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var id = Guid.NewGuid();

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.DeleteCocktailAsync(id);
                Assert.IsFalse(result);
            }
        }
    }
}
