﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.BarRatingServiceTests
{
    [TestClass]
    public class GetAllBarRatings_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBarRatings()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBarRatings));

            var barId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var userId2 = Guid.NewGuid();

            var barRating = new BarRating
            {
                BarId = barId,
                UserId = userId,
                Rating = 3
            };
            var barRating2 = new BarRating
            {
                BarId = barId,
                UserId = userId2,
                Rating = 5
            };

            var ratings = new List<BarRating> { barRating, barRating2 };

            var ratingsDTO = new List<BarRatingDTO>()
            {
                new BarRatingDTO
                {
                    BarId = barRating.BarId,
                    UserId = barRating.UserId,
                    User = "testuser",
                    Rating = barRating.Rating
                },
                new BarRatingDTO
                {
                    BarId = barRating2.BarId,
                    UserId = barRating2.UserId,
                    User = "testuser2",
                    Rating = barRating2.Rating
                }
            };

            var mockMapper = new Mock<IDTOMapper<BarRating, BarRatingDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<ICollection<BarRating>>())).Returns(ratingsDTO);

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.BarRatings.AddAsync(barRating);
                await arrangeContext.BarRatings.AddAsync(barRating2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarRatingService(assertContext, mockMapper.Object, mockFactory.Object);

                var result = await sut.GetAllBarRatingsAsync(barId);

                var list = result.ToList();
                for (int i = 0; i < list.Count; i++)
                {
                    Assert.AreEqual(ratings[i].UserId, list[i].UserId);
                    Assert.AreEqual(ratings[i].BarId, list[i].BarId);
                    Assert.AreEqual(ratings[i].Rating, list[i].Rating);
                }

                Assert.AreEqual(2, result.Count());               
            }
        }
    }
}
