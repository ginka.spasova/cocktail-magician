﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.BarServiceTests
{
    [TestClass]
    public class GetAllBarsAsync_Should
    {
        [TestMethod]
        public async Task Return_CorrectBars()
        {
            var options = Utilities.GetOptions(nameof(Return_CorrectBars));

            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
            };

            var bar2 = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "RockIt",
                Description = "So nice",
                Phone = "111-555-333",
                Address = "Sofia, Petko Karavelov 5",
            };

            var barsList = new List<Bar> { bar, bar2 };

            var bars = new List<BarDTO>()
            {
                new BarDTO
                {
                    Id = bar.Id,
                    Name = bar.Name,
                    Description = bar.Description,
                    Phone = bar.Phone,
                    Address = bar.Address
                },
                new BarDTO
                {
                    Id = bar2.Id,
                    Name = bar2.Name,
                    Description = bar2.Description,
                    Phone = bar2.Phone,
                    Address = bar2.Address
                }
            };
            var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<PaginatedList<Bar>>()))
                .Returns<PaginatedList<Bar>>(pt =>
                new PageTransferInfo<BarDTO>
                {
                    HasNextPage = pt.HasNextPage,
                    HasPreviousPage = pt.HasPreviousPage,
                    TotalPages = pt.TotalPages,
                    PageIndex = pt.PageIndex,
                    Collection = bars
                });

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Bars.AddAsync(bar);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.GetAllBarsAsync(1);

                var resultList = result.Collection.ToList();

                for (int i = 0; i < result.Collection.Count; i++)
                {
                    Assert.AreEqual(barsList[i].Name, resultList[i].Name);
                    Assert.AreEqual(barsList[i].Description, resultList[i].Description);
                    Assert.AreEqual(barsList[i].Phone, resultList[i].Phone);
                    Assert.AreEqual(barsList[i].Address, resultList[i].Address);
                }

                Assert.AreEqual(2, result.Collection.Count);
            }
        }

      // [TestMethod]
      // public async Task ThrowWhen_BarsNotFound()
      // {
      //     var options = Utilities.GetOptions(nameof(ThrowWhen_BarsNotFound));
      //
      //     var bar = new Bar
      //     {
      //         Id = Guid.NewGuid(),
      //         Name = "Terminal 1",
      //         Description = "Very cool",
      //         Phone = "111-222-333",
      //         Address = "Sofia, Angel Kanchev 1",
      //     };
      //
      //     var bar2 = new Bar
      //     {
      //         Id = Guid.NewGuid(),
      //         Name = "RockIt",
      //         Description = "So nice",
      //         Phone = "111-555-333",
      //         Address = "Sofia, Petko Karavelov 5",
      //     };
      //
      //     var bars = new List<BarDTO>()
      //     {
      //         new BarDTO
      //         {
      //             Id = bar.Id,
      //             Name = bar.Name,
      //             Description = bar.Description,
      //             Phone = bar.Phone,
      //             Address = bar.Address
      //         },
      //         new BarDTO
      //         {
      //             Id = bar2.Id,
      //             Name = bar2.Name,
      //             Description = bar2.Description,
      //             Phone = bar2.Phone,
      //             Address = bar2.Address
      //         }
      //     };
      //
      //     var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
      //     var mockFactory = new Mock<IFactory>();
      //
      //     mockMapper.Setup(x => x.MapToDTO(It.IsAny<ICollection<Bar>>())).Returns(bars);
      //
      //     var mockDateTimeProvider = new Mock<IDateTimeProvider>();
      //
      //     using (var assertContext = new CMDbContext(options))
      //     {
      //         //Act & Assert
      //         var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);
      //
      //         await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAllBarsAsync(1));
      //     }
      // }
    }
}
