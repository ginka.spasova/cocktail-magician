﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.BarServiceTests
{
    [TestClass]
    public class DeleteBarAsync_Should
    {
        [TestMethod]
        public async Task ReturnTrue_WhenBarIsDeleted()
        {
            var options = Utilities.GetOptions(nameof(ReturnTrue_WhenBarIsDeleted));

            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
            };

            var barDTO = new BarDTO
            {
                Id = bar.Id,
                Name = bar.Name,
                Description = bar.Description,
                Phone = bar.Phone,
                Address = bar.Address
            };

            var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Bar>())).Returns(barDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Bars.AddAsync(bar);
                await arrangeContext.SaveChangesAsync();
            }

            var id = bar.Id;

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.DeleteBarAsync(id);

                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_BarNotFound()
        {
            var options = Utilities.GetOptions(nameof(ReturnFalse_When_BarNotFound));

            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
            };

            var barDTO = new BarDTO
            {
                Id = bar.Id,
                Name = bar.Name,
                Description = bar.Description,
                Phone = bar.Phone,
                Address = bar.Address
            };

            var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockFactory.Setup(fact => fact.CreateBar(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                       .Returns(bar);

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Bar>())).Returns(barDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var id = bar.Id;

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.DeleteBarAsync(id);

                Assert.IsFalse(result);
            }
        }
    }
}
