﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.BarServiceTests
{
    [TestClass]
    public class FilterBarsByNameAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBars_FilteredByName()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBars_FilteredByName));

            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
            };

            var bar2 = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "RockIt",
                Description = "So nice",
                Phone = "111-555-333",
                Address = "Sofia, Petko Karavelov 5",
            };

            var bar3 = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "RockBar",
                Description = "Good",
                Phone = "111-551-333",
                Address = "Sofia, Petko Karavelov 15",
            };

            var bars = new List<BarDTO>()
            {
                new BarDTO
                {
                    Id = bar.Id,
                    Name = bar.Name,
                    Description = bar.Description,
                    Phone = bar.Phone,
                    Address = bar.Address
                },
                new BarDTO
                {
                    Id = bar2.Id,
                    Name = bar2.Name,
                    Description = bar2.Description,
                    Phone = bar2.Phone,
                    Address = bar2.Address
                }
            };
            var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<PaginatedList<Bar>>()))
               .Returns<PaginatedList<Bar>>(pt =>
               new PageTransferInfo<BarDTO>
               {
                   HasNextPage = pt.HasNextPage,
                   HasPreviousPage = pt.HasPreviousPage,
                   TotalPages = pt.TotalPages,
                   PageIndex = pt.PageIndex,
                   Collection = bars
               });

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Bars.AddAsync(bar);
                await arrangeContext.Bars.AddAsync(bar2);
                await arrangeContext.Bars.AddAsync(bar3);

                await arrangeContext.SaveChangesAsync();
            }

            var filterOption = "Rock";
            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.FilterBarsAsync(filterOption, 1);

                Assert.AreEqual(2, result.Collection.Count());
            }
        }

      // [TestMethod]
      // public async Task ThrowWhen_BarWithName_NotFound()
      // {
      //     var options = Utilities.GetOptions(nameof(ThrowWhen_BarWithName_NotFound));
      //
      //     var bar = new Bar
      //     {
      //         Id = Guid.NewGuid(),
      //         Name = "Terminal 1",
      //         Description = "Very cool",
      //         Phone = "111-222-333",
      //         Address = "Sofia, Angel Kanchev 1",
      //     };
      //
      //     var bar2 = new Bar
      //     {
      //         Id = Guid.NewGuid(),
      //         Name = "RockIt",
      //         Description = "So nice",
      //         Phone = "111-555-333",
      //         Address = "Sofia, Petko Karavelov 5",
      //     };
      //
      //     var bars = new List<BarDTO>()
      //     {
      //         new BarDTO
      //         {
      //             Id = bar.Id,
      //             Name = bar.Name,
      //             Description = bar.Description,
      //             Phone = bar.Phone,
      //             Address = bar.Address
      //         },
      //         new BarDTO
      //         {
      //             Id = bar2.Id,
      //             Name = bar2.Name,
      //             Description = bar2.Description,
      //             Phone = bar2.Phone,
      //             Address = bar2.Address
      //         }
      //     };
      //     var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
      //     var mockFactory = new Mock<IFactory>();
      //
      //     mockMapper.Setup(x => x.MapToDTO(It.IsAny<ICollection<Bar>>())).Returns(bars);
      //
      //     var mockDateTimeProvider = new Mock<IDateTimeProvider>();
      //
      //     var filterOption = "Rock";
      //     using (var assertContext = new CMDbContext(options))
      //     {
      //         //Act & Assert
      //         var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);
      //
      //         await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.FilterBarsAsync(filterOption, 1));
      //     }
      // }
    }
}
