﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Tests.FactoryTests
{
    [TestClass]
    public class CreateCocktailIngredient_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre"
            };

            var sut = new Factory();

            var result = sut.CreateCocktailIngredient(cocktail.Id, ingredient.Id);

            Assert.IsInstanceOfType(result, typeof(CocktailIngredient));
        }

        [TestMethod]
        public void ReturnCorrectCocktailIngredient()
        {
            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre"
            };

            var sut = new Factory();

            var result = sut.CreateCocktailIngredient(cocktail.Id, ingredient.Id);

            Assert.AreEqual(ingredient.Id, result.IngredientId);
            Assert.AreEqual(cocktail.Id, result.CocktailId);
        }
    }
}
