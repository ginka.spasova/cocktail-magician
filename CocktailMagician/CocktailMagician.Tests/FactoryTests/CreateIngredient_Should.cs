﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Tests.FactoryTests
{
    [TestClass]
    public class CreateIngredient_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var name = "Strawberry";
            var sut = new Factory();

            var result = sut.CreateIngredient(name);

            Assert.IsInstanceOfType(result, typeof(Ingredient));
        }

        [TestMethod]
        public void ReturnCorrectIngredient()
        {
            var name = "Strawberry";
            var sut = new Factory();

            var result = sut.CreateIngredient(name);

            Assert.AreEqual(name, result.Name);
        }
    }
}
