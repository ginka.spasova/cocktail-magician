﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Tests.FactoryTests
{
    [TestClass]
    public class CreateCocktail_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var name = "Manhattan";
            var description = "Very cool";

            var sut = new Factory();

            var result = sut.CreateCocktail(name, description);

            Assert.IsInstanceOfType(result, typeof(Cocktail));
        }

        [TestMethod]
        public void ReturnCorrectCocktail()
        {
            var name = "Manhattan";
            var description = "Very cool";

            var sut = new Factory();

            var result = sut.CreateCocktail(name, description);

            Assert.AreEqual(name, result.Name);
            Assert.AreEqual(description, result.Description);
        }
    }
}
