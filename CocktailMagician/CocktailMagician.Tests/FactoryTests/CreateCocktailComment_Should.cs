﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Tests.FactoryTests
{
    [TestClass]
    public class CreateCocktailComment_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var comment = "Very sweet";

            var sut = new Factory();

            var result = sut.CreateCocktailComment(comment);

            Assert.IsInstanceOfType(result, typeof(CocktailComment));
        }

        [TestMethod]
        public void ReturnCorrectComment()
        {
            var comment = "Very sweet";

            var sut = new Factory();

            var result = sut.CreateCocktailComment(comment);

            Assert.AreEqual(comment, result.Text);
        }
    }
}
