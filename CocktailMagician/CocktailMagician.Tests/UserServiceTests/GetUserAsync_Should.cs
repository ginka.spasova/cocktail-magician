﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.UserServiceTests
{
    [TestClass]
   public class GetUserAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectUser()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectUser));

            var user1 = new User { UserName = "TestUser1", Id = Guid.NewGuid(), LockoutEnabled = true };
            var mockMapper = new Mock<IDTOMapper<User, UserDTO>>();
            mockMapper.Setup(x => x.MapToDTO(It.IsAny<User>()))
                .Returns<User>(x => new UserDTO
                {
                    Name = x.UserName,
                    Id = x.Id,
                });

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.AddAsync(user1);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new UserService(assertContext, mockMapper.Object);
                var result = await sut.GetUserAsync(user1.Id);

                var dbUser = await assertContext.Users.FindAsync(user1.Id);

                Assert.AreEqual(dbUser.Id, result.Id);
                Assert.AreEqual(dbUser.UserName, result.Name);
            }
        }
        [TestMethod]
        public async Task ThrowsException_When_NoSuchUser()
        {
            var options = Utilities.GetOptions(nameof(ThrowsException_When_NoSuchUser));

            var user1 = new User { UserName = "TestUser1", Id = Guid.NewGuid(), LockoutEnabled = true };
            var mockMapper = new Mock<IDTOMapper<User, UserDTO>>();
            mockMapper.Setup(x => x.MapToDTO(It.IsAny<User>()))
                .Returns<User>(x => new UserDTO
                {
                    Name = x.UserName,
                    Id = x.Id,
                });

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.AddAsync(user1);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new UserService(assertContext, mockMapper.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetUserAsync(Guid.NewGuid()));
            }
        }
    }
}
