﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.UserServiceTests
{
    [TestClass]
   public class UnbanAsync_Should
    {
        [TestMethod]
        public async Task ReturnsTrueAndRemovesBan()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnsTrueAndRemovesBan));

            var user1 = new User { UserName = "TestUser1", Id = Guid.NewGuid(), LockoutEnabled = true, LockoutEnd = DateTimeOffset.MaxValue };
            var mockMapper = new Mock<IDTOMapper<User, UserDTO>>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.AddAsync(user1);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new UserService(assertContext, mockMapper.Object);
                var result = await sut.UnbanUserAsync(user1.Id);

                var dbUser = await assertContext.Users.FindAsync(user1.Id);

                Assert.IsTrue(result);
                Assert.IsTrue(dbUser.LockoutEnd == null);
            }
        }
        [TestMethod]
        public async Task ReturnsFalseWhenNoUserToUnban()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnsFalseWhenNoUserToUnban));

            var user1 = new User { UserName = "TestUser1", Id = Guid.NewGuid(), LockoutEnabled = true, LockoutEnd = DateTimeOffset.MaxValue };
            var mockMapper = new Mock<IDTOMapper<User, UserDTO>>();
            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new UserService(assertContext, mockMapper.Object);
                var result = await sut.UnbanUserAsync(user1.Id);

                var dbUser = await assertContext.Users.FindAsync(user1.Id);

                Assert.IsFalse(result);
            }

        }
    }
}
