﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.IngredientServiceTests
{
    [TestClass]
    public class DeleteIngredientAsync_Should
    {
        [TestMethod]
        public async Task ReturnTrue_When_IngredientNotInCocktail()
        {
            var options = Utilities.GetOptions(nameof(ReturnTrue_When_IngredientNotInCocktail));

            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();
            var mockFactory = new Mock<IFactory>();

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Ingredients.AddAsync(ingredient);
                await arrangeContext.SaveChangesAsync();
            }

            var id = ingredient.Id;
            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new IngredientService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.DeleteIngredientAsync(id);

                Assert.IsTrue(result);
            }
        }
        [TestMethod]
        public async Task ReturnFalse_When_IngredientIsInCocktail()
        {
            var options = Utilities.GetOptions(nameof(ReturnFalse_When_IngredientIsInCocktail));

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Rose Berry Bliss",
            };

            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine",
            };

            var cocktailIngredient = new CocktailIngredient
            {
                CocktailId = cocktail.Id,
                IngredientId = ingredient.Id,
            };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();
            var mockFactory = new Mock<IFactory>();

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.Ingredients.AddAsync(ingredient);
                await arrangeContext.CocktailIngredients.AddAsync(cocktailIngredient);
                await arrangeContext.SaveChangesAsync();
            }

            var id = ingredient.Id;
            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new IngredientService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.DeleteIngredientAsync(id);

                Assert.IsFalse(result);
            }
        }
    }
}
